<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\roleController;
use App\Http\Controllers\user\petitionController;
use App\Http\Controllers\admin\categoryController;
use App\Http\Controllers\admin\subcategoryController;
use App\Http\Controllers\admin\MembersController;
use App\Http\Controllers\admin\AmessengerController;
use App\Http\Controllers\Architecture\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.layouts.master');
})->name('main.page');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'dashboard','middleware'=>['auth' ,'admin']],function () {
   Route::get('/',[\App\Http\Controllers\HomeController::class,'adminpanel'])->name('adminpanel');
   Route::get('/setting',[\App\Http\Controllers\HomeController::class,'setting'])->name('setting');
   Route::put('/update/setting/{user}',[\App\Http\Controllers\HomeController::class,'update_setting'])->name('update.setting');
   Route::resource('/role',roleController::class);
   Route::get('/dashboard_notifications',[petitionController::class,'notifications'])->name('dashboard_notifications');
   Route::get('/notification/show/{petition}',[petitionController::class,'notificationShow'])->name('notification.show');
   Route::get('/notification/accept/{petition}',[petitionController::class,'notification_accept'])->name('dashboard.notification.accept');
   Route::post('/notification/cancel/{petition}',[petitionController::class,'notification_cancel'])->name('dashboard.notification.cancel');
   Route::resource('/category',categoryController::class);
   Route::resource('/subcategory',subcategoryController::class);
   Route::get('/architecture',[MembersController::class,'architecture'])->name('dashboard.architecture');
   Route::get('/users',[MembersController::class,'users'])->name('dashboard.users');
   Route::get('/receive',[ProductController::class,'receive'])->name('receive.product');
   Route::get('/receive/{product}',[ProductController::class,'product_show_receive'])->name('product.show_receive');
   Route::get('/product/press_accept/{product}',[ProductController::class,'press_accept'])->name('product.press_accept');
   Route::post('/product/cancel/{product}',[ProductController::class,'reason'])->name('product.reason');
   Route::get('/accept',[ProductController::class,'accept_product'])->name('accept.product');
   Route::get('/cancel',[ProductController::class,'cancel_product'])->name('cancel.product');
   Route::post('/product/block',[ProductController::class,'block_product'])->name('product.block');
   Route::post('/spam/architecture/{architect}',[App\Http\Controllers\Architecture\spamController::class,'architecture_spam'])->name('architecture.spam');
   Route::get('/nospam/architecture/{architect}',[App\Http\Controllers\Architecture\spamController::class,'nospam'])->name('architecture.nospam');
   Route::post('/spam/user/{user}',[App\Http\Controllers\Architecture\spamController::class,'user_spam'])->name('user.spam');
   Route::get('/nospam/user/{user}',[App\Http\Controllers\Architecture\spamController::class,'user_nospam'])->name('user.nospam');
   Route::get('/messenger',[AmessengerController::class,'create'])->name('messengers.messenger');
   Route::get('/chat/{messenger}',[AmessengerController::class,'chat_member'])->name('messengers.chat');
   Route::post('/admin/text/{account}',[AmessengerController::class,'admin_text_messenger'])->name('admin_text_messenger');
   Route::post('/search/',[AmessengerController::class,'search'])->name('messengers.search');
   Route::get('/contact',[App\Http\Controllers\Main\ContactController::class, 'receive_contact'])->name('receive.contact');
});
Route::group(['prefix' => 'arch','middleware'=>['auth' ,'architec']],function () {
    Route::get('/',[\App\Http\Controllers\HomeController::class,'architecpanel'])->name('architecpanel');
    Route::get('/architek/setting',[\App\Http\Controllers\HomeController::class,'archsetting'])->name('architek_setting');
    Route::put('/setting/{user}',[\App\Http\Controllers\HomeController::class,'archupdate'])->name('update');
    Route::resource('/product',ProductController::class);
//    Route::get('/product/receive',[ProductController::class,'productReceive'])->name('product.receive');
    Route::post('/select/category',[ProductController::class,'select_category'])->name('architec.category');
    Route::post('/select/subcategory',[ProductController::class,'select_subcategory'])->name('architec.subcategory');
    Route::get('/chat',[App\Http\Controllers\Admin\AmessengerController::class,'chat_with_admin'])->name('admin_chat');
    Route::post('/message/text',[App\Http\Controllers\Admin\AmessengerController::class,'architec_text_message'])->name('architec_text_message');
});


Route::group(['prefix'=>'use','middleware'=>['auth' , 'user']],function () {
    Route::get('/',[\App\Http\Controllers\HomeController::class,'userpanel'])->name('userpanel');
    Route::get('/setting',[\App\Http\Controllers\HomeController::class,'usersetting'])->name('user.setting');
    Route::put('/update/setting/{user}',[\App\Http\Controllers\HomeController::class,'userupdate'])->name('user.update');
    Route::resource('/petition',petitionController::class);
    Route::get('/chat',[App\Http\Controllers\Admin\AmessengerController::class,'chat_with_user'])->name('user_chat');
    Route::post('messenger/text',[App\Http\Controllers\Admin\AmessengerController::class,'user_text_messenger'])->name('user_text_messenger');
});
Route::get('/main/contact',[App\Http\Controllers\Main\ContactController::class,'contact'])->name('main_contact');
Route::post('/send',[App\Http\Controllers\Main\ContactController::class,'store'])->name('contact.store');
