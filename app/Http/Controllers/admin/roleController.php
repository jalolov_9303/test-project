<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\role;
use App\Models\User;
use Illuminate\Http\Request;

class roleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = role::select('roles.id','roles.role_id','roles.user_id','users.name','users.email')
            ->join('users','users.id' ,'=' , 'roles.user_id' )->get();
        return view('admin.role.view',compact('roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('admin.role.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'role_id' => 'required',
        ]);
        $role = new role();
        $user_id = $request->input('user_id');
        $role_id = $request->input('role_id');
        $role->user_id = $user_id;
        $role->role_id = $role_id;
        User::where('id' , '=' ,$user_id)->update([
            'role' => $role_id
        ]);
        $role->save();
        return redirect()->route('adminpanel');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(role $role)
    {
        $users =  User::where('id' , '=' , $role->user_id)->get();
        return view('admin.role.edit',compact('role','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, role $role)
    {
        $role_id = $request->input('role_id');
        User::where('id' ,'=' , $role->user_id)
            ->update([
                'role' => $role_id
            ]);
        $role ->role_id = $role_id;
        $role->update();
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(role $role)
    {
        //
    }
}
