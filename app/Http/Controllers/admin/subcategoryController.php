<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class subcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::select('subcategories.id','subcategories.subcategory_name','subcategories.created_at','subcategories.category_id','categories.category_name')
        ->join('categories','categories.id' , '=' , 'subcategories.category_id')->get();
        return view('admin.subcategories.view',compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.subcategories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'category_id' => 'required',
            'subcategory_name'=>'required'
        ]);
        $category_id = $request->input('category_id');
       $subcategories = new Subcategory();
       for ($i=0 ; $i<count($request->subcategory_name);$i++)
       {
           $subcategories = new Subcategory();
           $subcategories->category_id =$category_id;
           $subcategories->subcategory_name = $request->subcategory_name[$i];
           $subcategories->save();
       }

       return redirect()->back();
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {
        $categories = Category::all();
        return view('admin.subcategories.edit',compact('subcategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        $request->validate([
            'subcategory_name' => 'required'
        ]);
        $category_id = $request->input('category_id');
        $subcategory_name = $request->input('subcategory_name');
        $subcategory->category_id = $category_id;
        $subcategory->subcategory_name = $subcategory_name;
        $subcategory->update();
        return redirect()->route('subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        $subcategory->delete();
        return redirect()->route('subcategory.index');
    }
}
