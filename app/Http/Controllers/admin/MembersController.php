<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function architecture()
    {
        $architecture = User::where('role' , '=' ,2)->get();
        $architecture_count = User::where('role' , '=' ,2)->count();
        return view('admin.members.viewarchitector',compact('architecture','architecture_count'));
    }
    public function users()
    {
        $users = User::where('role' , '=' ,3)->get();
        $users_count = User::where('role' , '=' ,3)->count();
        return view('admin.members.viewuser',compact('users','users_count'));
    }
}
