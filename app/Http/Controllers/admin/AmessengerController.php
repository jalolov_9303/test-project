<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Amessenger;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AmessengerController extends Controller
{
    public function create()
    {
//        $sel_qwery = DB::select('select distinct(architec_id) from amessengers order by architec_id desc ');
//         dd($sel_qwery);
        $members = User::where('role','!=',1)->get();
        return view('admin.messengers.index',compact('members'));
    }
    public function chat_member($messenger)
    {
        $account = User::where('id','=',$messenger)->first();
        $old_messeges = Amessenger::where('architec_id' , '=',$messenger)->get();
        return view('admin.messengers.chat',compact('account','old_messeges'));
    }

    public function admin_text_messenger(Request $request,$account)
    {
        $request->validate([
            'admin_message'=>'required'
        ]);
        $amessege = new Amessenger();
        $amessege->admin_id = Auth::user()->id;
        $amessege->architec_id = $account;
        $amessege->admin_messege =$request->input('admin_message');
        $amessege->save();
        return redirect()->route('messengers.chat',$account);
    }
    public function search(Request $request)
    {
        $result = DB::table('users')
            ->where('name','like','%'.$request->search.'%')
            ->where('role' , '!=',1)
            ->get();
        return response()->json([
            'status' => true,
            'result' => $result
        ]);
    }
    public function chat_with_admin()
    {
        $admin_names =  User::where('role','=',1)->get();

        $all_messengers_with_admin = Amessenger::where('architec_id','=',Auth::user()->id)->get();
        return view('architec.messengers.chat',compact('all_messengers_with_admin','admin_names'));
    }
    public function architec_text_message(Request $request)
    {
        $request->validate([
            'architec_message' => 'required'
        ]);
        $amessege = new  Amessenger();
        $amessege->admin_id = 1;
        $amessege->architec_id = Auth::user()->id;
        $amessege->admin_messege = null;
        $amessege->architec_messege = $request->input('architec_message');
        $amessege->save();
        return redirect()->route('admin_chat');
    }
    public function chat_with_user()
    {
        $admin_names = User::where('role','=',1)->get();
        $all_messengers_user = Amessenger::where('architec_id','=',Auth::user()->id)->get();
        return view('user.messengers.chat',compact('all_messengers_user','admin_names'));
    }
    public function user_text_messenger(Request $request)
    {
        $request->validate([
            'user_message' => 'required'
        ]);
        $amessenger = new Amessenger();
        $amessenger->admin_id = 1;
        $amessenger->architec_id = Auth::user()->id;
        $amessenger->architec_messege = $request->input('user_message');
        $amessenger->save();
        return redirect()->route('user_chat');
    }
}








