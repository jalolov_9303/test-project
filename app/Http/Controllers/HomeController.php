<?php

namespace App\Http\Controllers;

use App\Models\petition;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
//    public function index()
//    {
//        return view('home');
//    }
      public function adminpanel()
      {
          return view('admin.index');
      }
      public function architecpanel()
      {
          return view('architec.index');
      }
      public function userpanel()
      {
          return view('user.index');
      }
      public function setting()
      {
          return view('admin.setting');
      }
      public function update_setting(User $user , Request $request)
      {
            $request->validate([
                'name' => 'required',
                'email' => 'required|unique:users,email,'.$user->id,
                'old_password' => ['required',function($attribute ,$value ,$fail)
                {
                    if (!Hash::check($value,Auth::user()->password))
                    {
                      return $fail('old password xato')  ;
                    }
                }],

                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required',

            ]);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->update();
            return redirect()->route('adminpanel');
      }
        public function archsetting()
        {
            return view('architec.setting');
        }
        public function archupdate(User $user,Request $request)
        {
            $request->validate([
                'name' => 'required',
                'email' => 'required|unique:users,email,'.$user->id,
                'old_password' => ['required',function($attribute,$value,$fail)
                {
                    if (!Hash::check($value,Auth::user()->password))
                    {
                        return $fail('ald password xato');
                    }
                }],
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required',
            ]);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->update();
            return redirect()->route('architecpanel');
        }

        public function usersetting()
        {
            return view('user.setting');
        }
        public function userupdate(User $user ,Request $request)
        {
            $request->validate([
                'name' => 'required',
                'email' => 'required|unique:users,email,'.$user->id,
                'old_password' => ['required',function($attribute,$value,$fail)
                {
                    if (!Hash::check($value,Auth::user()->password))
                    {
                        return $fail('ald password xato');
                    }
                }],
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required',
            ]);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->update();
            return redirect()->route('userpanel');

        }



}
