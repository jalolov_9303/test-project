<?php

namespace App\Http\Controllers\Architecture;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class spamController extends Controller
{
    public function architecture_spam(Request $request,$architect)
    {
            User::where('id','=',$architect)
                ->update([
                    'spam_status' => 1,
                    'spam_reason'=>$request->spam_reason
                ]);
            return redirect()->route('dashboard.architecture');
    }
    public function nospam($architect)
    {
        User::where('id' ,  '=' ,$architect)
            ->update([
                'spam_status' => 0,
                'spam_reason' => 'No spam'
            ]);
        return redirect()->route('dashboard.architecture');

    }
    public function user_spam(Request $request ,$user)
    {
        User::where('id' , '=' , $user)
            ->update([
                'spam_status' => 1,
                'spam_reason' => $request->status_reason
            ]);
        return redirect()->route('dashboard.users');
    }
    public function user_nospam($user)
    {
      User::where('id' , '=' , $user)
          ->update([
              'spam_status' => 0,
              'spam_reason' => 'No spam'
          ]);
        return redirect()->route('dashboard.users');
    }

}
