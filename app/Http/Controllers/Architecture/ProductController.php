<?php

namespace App\Http\Controllers\Architecture;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as InterventionImage;
use Monolog\Handler\IFTTTHandler;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select('products.id','products.model_name','products.model_price','products.model_img_big','products.model_img_medium','products.model_img_small','products.model_file','products.model_size','products.model_description','products.created_at',
        'categories.category_name','subcategories.subcategory_name')
            ->join('categories','categories.id' , '=' , 'products.category_id')
            ->join('subcategories','subcategories.id' , '=' , 'products.subcategory_id')->get();
        return view('architec.products.view',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('architec.products.create',compact('categories','subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'model_name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'model_price' => 'required',
//            'model_img' => 'required|array',
            'model_img.*' => 'mimes:jpeg,png,jpg,svg',
//            'model_file' => 'required|mimes:max,blend,obj,fbx',
            'model_description' => 'required',
        ]);

        $org_images = '';
        $big_images = '';
        $medium_images = '';
        $small_images = '';

        $imageSizes =  config('images.sizes');
        $model_name = $request->input('model_name');
        $category_id = $request->input('category_id');
        $subcategory_id = $request->input('subcategory_id');
        $model_price = $request->input('model_price');
        $model_description = $request->input('model_description');
        $model_size = $request->file('model_file')->getSize();

        for ($i=0; $i<count($request->file('model_img'));$i++)
        {
            $all_img  = $request->file('model_img')[$i];
            $extension = $all_img->getClientOriginalExtension();
            $fileName = time();
            $big = $fileName.$i.'_'.$imageSizes['big']['width'].'x'.
                $imageSizes['big']['height'].'.'.$extension;

            $medium = $fileName.$i.'_'.$imageSizes['medium']['width'].'x'.
                $imageSizes['medium']['height'].'.'.$extension;

            $small = $fileName.$i.'_'.$imageSizes['small']['width'].'x'.
                $imageSizes['small']['height'].'.'.$extension;

            $big_images .= $big.',';
            $medium_images .= $medium.',';
            $small_images .= $small.',';

            InterventionImage::make($all_img)
                ->fit($imageSizes['big']['width'],$imageSizes['big']['height'])
                ->save(public_path('architec/models/images/big/').$big);

            InterventionImage::make($all_img)
                ->fit($imageSizes['medium']['width'],$imageSizes['medium']['height'])
                ->save(public_path('architec/models/images/medium/').$medium);

            InterventionImage::make($all_img)
                ->fit($imageSizes['small']['width'],$imageSizes['small']['height'])
                ->save(public_path('architec/models/images/small/').$small);
            $all_img->move(public_path('architec/models/images/orginals'),$fileName.$i.'.'.$extension);

        }
        $model_file = $request->file('model_file');
        $extension_file = $model_file->getClientOriginalExtension();
        $ordinal_name = time().'.'.$extension_file;
        if ($extension_file == 'max')
        {
            $model_file->move(public_path('architec/models/files/max/'),$ordinal_name);
        }
        elseif ($extension_file == 'blend')
        {
            $model_file->move(public_path('architec/models/files/blend/'),$ordinal_name);
        }
        elseif ($extension_file == 'fbx')
        {
            $model_file->move(public_path('architec/models/files/fbx/'),$ordinal_name);
        }
        elseif ($extension_file == 'ojb')
        {
            $model_file->move(public_path('architec/models/files/ojb/'),$ordinal_name);
        }

        $product = new Product();
        $product->user_id = Auth::user()->id;
        $product->category_id = $category_id;
        $product->subcategory_id = $subcategory_id;
        $product->model_name = $model_name;
        $product->model_price = $model_price;
        $product->model_img_big = $big_images;
        $product->model_img_org  = $org_images;
        $product->model_img_medium = $medium_images;
        $product->model_img_small = $small_images;
        $product->model_file = $ordinal_name;
        $product->model_size = $model_size;
        $product->model_type = $extension_file;
        $product->model_description = $model_description;
        $product->save();
        return redirect()->route('product.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $model_img_big = explode(',',$product->model_img_big);
        $model_img_medium = explode(',',$product->model_img_medium);
        $model_img_small = explode(',',$product->model_img_small);
        return view('architec.products.show',compact('product','model_img_big','model_img_medium','model_img_small'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $model_img_small = explode(',',$product->model_img_small);
        return view('architec.products.edit',compact('product','categories','subcategories','model_img_small'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'model_name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'model_price' => 'required',
            'model_img.*' => 'mimes:jpeg,png,jpg,svg',
            'model_file.*' =>'mimes:max,fbx,ojb,blend',
            'model_description' => 'required',
        ]);

        $imageSizes =  config('images.sizes');
        $model_name = $request->input('model_name');
        $category_id = $request->input('category_id');
        $subcategory_id = $request->input('subcategory_id');
        $model_price = $request->input('model_price');
        $model_description = $request->input('model_description');

        if ($request->hasFile('model_img'))
        {
            $all_img_big = explode(',',$product->model_img_big);
            $all_img_medium = explode(',',$product->model_img_medium);
            $all_img_small = explode(',',$product->model_img_small);
            $old_images_org = explode(',',$product->model_img_org);

            for ($i=0; $i<count($all_img_big)-1;$i++)
            {
                File::delete(public_path('architec/models/images/big/'.$all_img_big[$i]));
                File::delete(public_path('architec/models/images/medium/'.$all_img_medium[$i]));
                File::delete(public_path('architec/models/images/small/'.$all_img_small[$i]));
                File::delete(public_path('architec/models/images/orginals'.$old_images_org[$i]));
            }
            $big_images = '';
            $medium_images = '';
            $small_images = '';
            $org_images = '';

            for ($i=0; $i<count($request->file('model_img'));$i++)
            {
                $all_img  = $request->file('model_img')[$i];
                $extension = $all_img->getClientOriginalExtension();
                $fileName = time();
                $org_images .=$fileName.$i.'.'.$extension.',';

                $big = $fileName.$i.'_'.$imageSizes['big']['width'].'x'.
                    $imageSizes['big']['height'].'.'.$extension;

                $medium = $fileName.$i.'_'.$imageSizes['medium']['width'].'x'.
                    $imageSizes['medium']['height'].'.'.$extension;

                $small = $fileName.$i.'_'.$imageSizes['small']['width'].'x'.
                    $imageSizes['small']['height'].'.'.$extension;

                $big_images .= $big.',';
                $medium_images .= $medium.',';
                $small_images .= $small.',';
                InterventionImage::make($all_img)
                    ->fit($imageSizes['big']['width'],$imageSizes['big']['height'])
                    ->save(public_path('architec/models/images/big/').$big);

                InterventionImage::make($all_img)
                    ->fit($imageSizes['medium']['width'],$imageSizes['medium']['height'])
                    ->save(public_path('architec/models/images/medium/').$medium);

                InterventionImage::make($all_img)
                    ->fit($imageSizes['small']['width'],$imageSizes['small']['height'])
                    ->save(public_path('architec/models/images/small/').$small);
                $all_img->move(public_path('architec/models/images/orginals'),$fileName.$i.'.'.$extension);

            }

        }
        else{
            $big_images = $product->model_img_big;
            $medium_images = $product->model_img_medium;
            $small_images = $product->model_img_small;
            $org_images = $product->model_img_org;
        }

        if ($request->hasFile('model_file'))
        {
            File::delete(public_path('architec/models/files/'.$product->model_type.'/'.$product->model_file));
            $model_size = $request->file('model_file')->getSize();
            $model_file = $request->file('model_file');
            $extension_file = $model_file->getClientOriginalExtension();
            $ordinal_name = time().'.'.$extension_file;
            if ($extension_file == 'max')
            {
                $model_file->move(public_path('architec/models/files/max/'),$ordinal_name);
            }
            elseif ($extension_file == 'blend')
            {
                $model_file->move(public_path('architec/models/files/blend/'),$ordinal_name);
            }
            elseif ($extension_file == 'fbx')
            {
                $model_file->move(public_path('architec/models/files/fbx/'),$ordinal_name);
            }
            elseif ($extension_file == 'ojb')
            {
                $model_file->move(public_path('architec/models/files/ojb/'),$ordinal_name);
            }

        }
        else{
            $model_size= $product->model_size;
            $ordinal_name =$product->model_file;
            $extension_file = $product->model_type;
        }
        $product->user_id = Auth::user()->id;
        $product->category_id = $category_id;
        $product->subcategory_id = $subcategory_id;
        $product->model_name = $model_name;
        $product->model_price = $model_price;
        $product->model_img_org = $org_images;
        $product->model_img_big = $big_images;
        $product->model_img_medium = $medium_images;
        $product->model_img_small = $small_images;
        $product->model_file = $ordinal_name;
        $product->model_size = $model_size;
        $product->model_type = $extension_file;
        $product->model_description = $model_description;
        $product->update();
        return redirect()->route('product.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


//    select
    public function select_category(Request $request)
    {
        return response()->json([
            'status' => true,
            'subcategories' => Subcategory::where('category_id','=',$request->category_id)->get()
        ]);
    }
    public function select_subcategory(Request $request)
    {
        return response()->json([
            'status' => true,
            'categories' => Category::all(),
            'category_id' => Subcategory::select('category_id')->where('id','=',$request->subcategory_id)->first()
        ]);
    }


    public function receive()
    {
        $products = Product::where('status' , '=',0)->get();
        return view('admin.models.receive',compact('products'));
    }
    public function product_show_receive(Product $product)
    {
        $model_img_big = explode(',',$product->model_img_big);
        $model_img_medium = explode(',',$product->model_img_medium);
        $model_img_small = explode(',',$product->model_img_small);
        return view('admin.models.show',compact('product','model_img_big','model_img_medium','model_img_small'));
    }
    public function press_accept(Product $product)
    {
        Product::where('id','=',$product->id)
            ->update([
                'status' => 1,
                'block' => 1,
                'status_reason' => 'Congratulations'
            ]);
        return redirect()->route('receive.product');
    }

    public function reason(Request $request,Product $product)
    {
        Product::where('id' , '=' , $product->id)
            ->update([
                'status' => 2,
                'status_reason' =>$request->status_reason
            ]);
        return redirect()->route('receive.product');
    }
    public function accept_product()
    {
        $products = Product::where('status' , '=' , 1)->get();
        return view('admin.models.accept',compact('products'));
    }
    public function cancel_product()
    {
        $products = Product::where('status' , '=' , 2)->get();
        return view('admin.models.cancel',compact('products'));
    }

    public function block_product(Request $request)
    {
        Product::where('id','=',$request->product_id)
            ->update([
                'block'=>$request->status
            ]);
        return response()->json([
            'status' => true
        ]);
    }


}
