<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\petition;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class petitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $petitions = petition::where('user_id' , '=' , Auth::user()->id)->select('petitions.id','petitions.petition_text','petitions.result_description','petitions.portfolio_link','petitions.status','users.name')
        ->join('users','users.id' , '=' , 'petitions.user_id')->get();
        return view('user.petition.view',compact('petitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.petition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'petition_text' => 'required'
        ]);
        $petition = new petition();
        $petition->petition_text = $request->input('petition_text');
        $petition->portfolio_link = $request->input('portfolio_link');
        $petition->user_id = Auth::user()->id;
        $petition->status = 0;
        $petition->save();
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\petition  $petition
     * @return \Illuminate\Http\Response
     */
    public function show(petition $petition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\petition  $petition
     * @return \Illuminate\Http\Response
     */
    public function edit(petition $petition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\petition  $petition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, petition $petition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\petition  $petition
     * @return \Illuminate\Http\Response
     */
    public function destroy(petition $petition)
    {
        //
    }
    public function notifications()
    {
        $petitions = petition::where('status' , '=' , 0)
            ->select('petitions.id','petitions.petition_text','petitions.portfolio_link','petitions.status','petitions.user_id','users.name')
            ->join('users','users.id' , '=' , 'petitions.user_id')
            ->orderBy('id','desc')
            ->get();
        return view('admin.petition.view',compact('petitions'));
    }
    public function notificationShow(petition $petition)
    {
        return view('admin.petition.show',compact('petition'));
    }
    public function notification_accept(petition $petition)
    {
        petition::where('id' , '=',$petition->id)
            ->update([
                'status' => 1
            ]);
        User::where('id' , '=' ,$petition->user_id)
            ->update([
                'role' => 2
            ]);
        return redirect()->route('dashboard_notifications');
    }
    public function notification_cancel(Request $request,petition $petition)
    {
        $resalt = $request->input('result_description');
         petition::where('id' , '=',$petition->id)
            ->update([
                'status' => 2,
                'result_description' =>$resalt
            ]);

        return redirect()->route('dashboard_notifications');
    }

}
