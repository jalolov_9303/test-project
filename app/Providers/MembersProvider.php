<?php

namespace App\Providers;

use App\views\MembersComposer;
use Illuminate\Support\ServiceProvider;

class MembersProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.messengers.chat',MembersComposer::class);
    }
}
