<?php

namespace App\Providers;

use App\views\SubcategoryComposer;
use Illuminate\Support\ServiceProvider;

class SubcategoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('main.layouts.master',SubcategoryComposer::class);
    }
}
