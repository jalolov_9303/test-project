<?php

namespace App\Providers;

use App\views\ReceiveComposer;
use Illuminate\Support\ServiceProvider;

class ReceiveProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.layouts.master',ReceiveComposer::class);
    }
}
