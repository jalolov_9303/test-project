<?php

namespace App\views;

use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\View\View;

class SubcategoryComposer
{
public function compose(View $view)
{
    $subcategory_count = [];
    $subcategories = Subcategory::all();
    for ($i=0;$i<count($subcategories);$i++)
    {
        $count = Product::where('subcategory_id','=',$subcategories[$i]->id)->count();
        $subcategory_count[$i] = $count;
    }
    $view->with([
        'subcategories' => $subcategories,
        'subcategory_count' =>$subcategory_count,
    ]);
}
}
