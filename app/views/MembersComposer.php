<?php

namespace App\views;

use App\Models\User;
use Illuminate\View\View;

class MembersComposer
{
    public function compose(View $view )
    {
        $members = User::where('role','!=',1)->get();

        $view->with([
            'messengers'=>$members
        ]);
    }
}
