<?php

namespace App\views;

use App\Models\petition;
use Illuminate\View\View;

class PetitionComposer
{
    public function compose(View $view)
    {
        $petitions = petition::where('status' ,'=',0)
            ->select('petitions.id','petitions.petition_text','petitions.portfolio_link','petitions.status','petitions.user_id','users.name')
            ->join('users','users.id' , '=' , 'petitions.user_id')
            ->orderBy('id','desc')
            ->limit(3)
            ->get();
        $petition_count = petition::where('status','=',0)->count();

        $view->with([
            'petitions' => $petitions,
            'petition_count' => $petition_count
        ]);
    }
}
