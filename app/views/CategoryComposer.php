<?php

namespace App\views;

use App\Models\Category;
use App\Models\Product;
use Illuminate\View\View;

class CategoryComposer
{
  public function compose(View $view)
  {
      $category_count = [];
      $categories = Category::all();
      for ($i=0;$i<count($categories);$i++)
      {
          $ctn = Product::where('category_id' ,'=',$categories[$i]->id)->count();
          $category_count[$i] = $ctn;
      }
      $view->with([
          'categories' =>$categories,
          'category_count' => $category_count,
      ]);
  }
}
