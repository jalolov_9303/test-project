<?php

namespace App\views;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductComposer
{
    public function compose(View $view)
    {
//        $product_types = Product::select('model_type')->groupBy('model_type')->get();
           $product_types = DB::select('select model_type,count(model_type) as count from products group by model_type');
        $view->with([
            'product_types' => $product_types,
        ]);
    }
}
