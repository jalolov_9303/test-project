@extends('user.layouts.master')
@section('title','user setting')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-xl-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h5>Setting {{\Illuminate\Support\Facades\Auth::user()->name}} </h5>
                        </div>
                        <div class="card-body">
                            <form action="{{route('user.update',\Illuminate\Support\Facades\Auth::user()->id)}}" method="post">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input value="{{\Illuminate\Support\Facades\Auth::user()->name}}"  name="name" class="form-control  @error('name') is-invalid @enderror" type="text" required="" placeholder="name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mt-3">
                                    <label >Email Address</label>
                                    <input value="{{\Illuminate\Support\Facades\Auth::user()->email}}"  name="email" class="form-control @error('email') is-invalid @enderror" type="email" required="" placeholder="Test@gmail.com">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mt-3">
                                    <label>Old Password</label>
                                    <div class="form-input position-relative">
                                        <input class="form-control @error('old_password') is-invalid @enderror" type="password" name="old_password" required="" placeholder="*********">
                                        @error('old_password')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <label>Password</label>
                                    <div class="form-input position-relative">
                                        <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" required="" placeholder="*********">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <label >Confirm Password</label>
                                    <div class="form-input position-relative">
                                        <input name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" type="password"  required="" placeholder="*********">

                                        @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="card-footer text-start">
                                    <button class="btn btn-primary" data-bs-original-title="" title="">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
