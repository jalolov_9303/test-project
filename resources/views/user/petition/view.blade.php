@extends('user.layouts.master')
@section('title' , 'view petition')
@section('content')
    <div class="card">
        <h5 class="card-header">user petition</h5>
        <div class="table-responsive text-nowrap">
            <table class="table">
                <thead class="table-dark">
                <tr>
                    <th style="color: white">№</th>
                    <th style="color: white">Name</th>
                    <th style="color: white">petition text</th>
                    <th style="color: white">Portfolio link</th>
                    <th style="color: white">Status</th>
                    <th style="color: white">Result</th>
{{--                    <th style="color: white">Actions</th>--}}
                </tr>
                </thead>
                @foreach($petitions as $key=>$petition)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$petition->name}}</td>
                            <td>{{substr($petition->petition_text,0 ,15)}} ...</td>
                            <td>
                                {{substr($petition->portfolio_link,0 ,15)}} ...
                            </td>
                            <td>
                                @if($petition->status == 0)
                                    <span class="btn btn-warning">checking</span>
                                @elseif($petition->status == 1)
{{--                                    <span class="badge bg-label-success me-1">Accept</span>--}}
                                    <span class="btn btn-success">Accept</span>
                                @elseif($petition->status == 2)
{{--                                    <span class="badge bg-label-danger me-1">cancel</span>--}}
                                    <span class="btn btn-danger">Cancel</span>
                                @endif
                            </td>
                            <td>
                                <div class="row gy-3">
                                        <div class="col-lg-4 col-md-6">
                                                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#basicModal">
                                                    result
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="basicModal" tabindex="-1" style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel1">Message from Administrator</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col mb-3">
                                                                        <p>{{$petition->result_description}}</p>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger-gradien" data-bs-dismiss="modal">
                                                                    Close
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                </div>
                            </td>

{{--                            <td>--}}
{{--                                <a class="btn btn-primary" href="">Edit</a>--}}
{{--                            </td>--}}
                        </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
