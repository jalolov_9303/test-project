@extends('user.layouts.master')
@section('title','Petition send user')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <h5 class="card-header">You can send petition to administrator for add to role Architector</h5>
                <div class="card-body">
                     <form action="{{route('petition.store')}}" method="post" enctype="multipart/form-data" >
                         @csrf
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1" >Petition text</label>
                            <textarea placeholder="petition text" name="petition_text" class="form-control mt-2   @error('petition_text') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                            @error('petition_text')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group mt-2">
                            <label for="defaultFormControlInput">Portfolio link</label>
                            <input name="portfolio_link" type="text" class="form-control mt-2  @error('portfolio_link') is-invalid @enderror" id="defaultFormControlInput" placeholder="https//" aria-describedby="defaultFormControlHelp">
                            @error('portfolio_link')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                         <div class="mt-3">
                             <input type="submit" class="btn btn-primary" value="submit" >
                         </div>
                     </form>
                </div>
            </div>
        </div>
    </div>
@endsection
