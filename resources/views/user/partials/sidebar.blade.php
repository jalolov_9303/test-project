<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper">
            <a href="{{route('userpanel')}}">
                <img class="img-fluid for-light" src="{{asset('user/assets/images/logo/logo.png')}}" alt="">
                <img class="img-fluid for-dark" src="{{asset('user/assets/images/logo/logo_dark.png')}}" alt="">
            </a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"></i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="#"><img class="img-fluid" src="{{asset('user/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="#"><img class="img-fluid" src="{{asset('user/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>                            Petition
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('use/petition/create')|| request()->is('use/petition') )? 'd-block' : ''}}">
                            <li>
                                <a style="color: {{request()->is('use/petition/create')? 'blue' : ''}}"  href="{{route('petition.create')}}">
                                    Add petition
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('use/petition')? 'blue' :''}}"  href="{{route('petition.index')}}">
                                    View petition
                                </a>
                            </li>
                        </ul>
                    </li>





                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
