@extends('admin.layouts.master')
@section('title' , 'messenger')
@section('content')
    <div class="row">
        <div class="col call-chat-sidebar col-sm-12">
            <div class="card mt-4">
                <div class="card-body chat-body">
                    <div class="chat-box">
                        <!-- Chat left side Start-->
                        <div class="chat-left-aside">
                            <div class="media">
                                <img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                <div class="about">
                                    <div class="name f-w-600">{{\Illuminate\Support\Facades\Auth::user()->name .' '. \Illuminate\Support\Facades\Auth::user()->lname}}</div>
                                    <div class="status">Status...</div>
                                </div>
                            </div>
                            <div class="people-list" id="people-list">
                                <div class="search">
                                        <div class="mb-3">
                                            <input name="search" id="search" class="form-control" type="search" placeholder="Search" data-bs-original-title="" title=""><i class="fa fa-search"></i>
                                        </div>
                                </div>
                                <ul class="list" id="members_list">
                                    @foreach($members as $member)
                                        <li class="clearfix">
                                            <a href="{{route('messengers.chat',$member)}}">
                                                <img class="rounded-circle user-image"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">

                                                <div class="status-circle away"></div>
                                            <div class="about">
                                                <div class="name">{{$member->name  }}</div>
                                                <div class="status">
                                                    @if($member->role ==2)
                                                        Architecture
                                                    @elseif($member->role == 3)
                                                        User
                                                    @endif
                                                </div>
                                            </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- Chat left side Ends-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function ()
        {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#search').keyup(function ()
            {
                var search =$('#search').val();
                var members_list =$('#members_list');
            $.ajax({
                url:'{{route('messengers.search')}}',
                method:'POST',
                data: {
                    search:search,
                },
                success:function (response)
                {
                    console.log(response.result)
                    var role ='';

                    $('#members_list').html("");
                    for (var i=0; i<response.result.length;i++)
                    {
                        if (response.result[i].role == 2)
                        {
                            role = 'Architecture'
                        }else{
                            role='user'
                        }
                        $('#members_list').append(
                            `
                            <li class="clearfix">
                              <a href="">
                                <img class="rounded-circle user-image"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                  <div class="status-circle away"></div>
                                    <div class="about">
                                      <div class="name">${response.result[i].name}</div>
                                         <div class="status">
                                              ${role}
                                        </div>
                                  </div>
                              </a>
                            </li>
`
                        )
                    }
                }
            });
            });

        });
    </script>
@endpush

