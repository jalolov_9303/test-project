@extends('admin.layouts.master')
@section('title' , 'chat messenger')
@section('content')
    <div class="row">
        <div class="col call-chat-sidebar col-sm-12">
            <div class="card mt-4">
                <div class="card-body chat-body">
                    <div class="chat-box">
                        <!-- Chat left side Start-->
                        <div class="chat-left-aside">
                            <div class="media">
                                <img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                <div class="about">
                                    <div class="name f-w-600">{{\Illuminate\Support\Facades\Auth::user()->name .' '. \Illuminate\Support\Facades\Auth::user()->lname}}</div>
                                    <div class="status">Status...</div>
                                </div>
                            </div>
                            <div class="people-list" id="people-list">
                                <div class="search">
                                    <div class="mb-3">
                                        <input name="search" id="search" class="form-control" type="search" placeholder="Search" data-bs-original-title="" title=""><i class="fa fa-search"></i>
                                    </div>
                                </div>
                                <ul class="list" id="members_list">
                                    @foreach($messengers as $messenger)
                                        <li class="clearfix">
                                            <a href="{{route('messengers.chat',$messenger)}}">
                                                <img class="rounded-circle user-image"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">

                                                <div class="status-circle away"></div>
                                                <div class="about">
                                                    <div class="name">{{$messenger->name  }}</div>
                                                    <div class="status">
                                                        @if($messenger->role ==2)
                                                            Architecture
                                                        @elseif($messenger->role == 3)
                                                            User
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- Chat left side Ends-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col call-chat-body">
            <div class="card mt-4">
                <div class="card-body p-0">
                    <div class="row chat-box">
                        <!-- Chat right side start-->
                        <div class="col pe-0 chat-right-aside">
                            <!-- chat start-->
                            <div class="chat">
                                <!-- chat-header start-->
                                <div class="chat-header clearfix"><img class="rounded-circle"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                    <div class="about">
                                        <div class="name">{{$account->name}}<span class="font-primary f-12">Typing...</span></div>
                                        <div class="status">Last Seen 3:55 PM</div>
                                    </div>
                                    <ul class="list-inline float-start float-sm-end chat-menu-icons">
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-search"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-clip"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-headphone-alt"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-video-camera"></i></a></li>
                                        <li class="list-inline-item toogle-bar"><a href="#" data-bs-original-title="" title=""><i class="icon-menu"></i></a></li>
                                    </ul>
                                </div>
                                <!-- chat-header end-->
                                <div class="chat-history chat-msg-box custom-scrollbar">
                                    <ul>
                                        @foreach($old_messeges as $old_messege)
                                            @if($old_messege->admin_messege == null)
                                            <li>
                                                <div class="message my-message"><img class="rounded-circle float-start chat-user-img img-30"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                                    <div class="message-data text-end"><span class="message-data-time">{{substr($old_messege->created_at,11,17)}}</span></div>{{$old_messege->architec_messege}}
                                                </div>
                                            </li>
                                            @elseif($old_messege->architec_messege == null)
                                                <li class="clearfix">
                                                    <div class="message other-message pull-right"><img class="rounded-circle float-end chat-user-img img-30"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                                        <div class="message-data"><span class="message-data-time">{{substr($old_messege->created_at,11,17)}}</span></div>   {{$old_messege->admin_messege}}
                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- end chat-history-->
                                <div class="chat-message clearfix">
                                    <div class="row">
                                        <form action="{{route('admin_text_messenger',$account->id)}}" method="post">
                                            @csrf
                                        <div class="col-xl-12 d-flex">
                                            <div class="input-group text-box">
                                                <input autocomplete="off" class="form-control  @error('admin_message') is-invalid @enderror" id="message-to-send" type="text" name="admin_message" placeholder="Type a message......" data-bs-original-title="" title="">
                                                <button class="btn-primary" type="submit" data-bs-original-title="" title="">Send</button>
                                            </div>

                                                @error('admin_message')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                                @enderror

                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function ()
        {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
{{--            var members_chat ="{{route('members_chat',':member')}}"--}}

            $('#search').keyup(function ()
            {
                var search =$('#search').val();
                var members_list = $('#members_list');
                $.ajax({
                    url:'{{route('messengers.search')}}',
                    method:'POST',
                    data: {
                        search:search,
                    },
                    success:function (response)
                    {
                        var role ='';

                        $('#members_list').html("");
                        for (var i=0; i<response.result.length;i++)
                        {
                            if (response.result[i].role == 2)
                            {
                                role = 'Architecture'
                            }else{
                                role='user'
                            }
                            $('#members_list').append(
                                `
                            {{--<li class="clearfix">--}}
                            {{--  <a href="${members_chat.replace(':member',response.result[i].id)}">--}}
                            {{--    <img class="rounded-circle user-image"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">--}}
                            {{--      <div class="status-circle away"></div>--}}
                            {{--        <div class="about">--}}
                            {{--          <div class="name">${response.result[i].name}</div>--}}
                            {{--             <div class="status">--}}
                            {{--                  ${role}--}}
                            {{--            </div>--}}
                            {{--      </div>--}}
                            {{--  </a>--}}
                            {{--</li>--}}
`
                            )
                        }
                    }
                })
            });

        });
    </script>
@endpush
