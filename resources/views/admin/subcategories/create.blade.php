@extends('admin.layouts.master')
@section('title' , 'Add subcategory')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3 ">
                    <div class="card-header">
                        <h5 class="card-title">Add category</h5>
                    </div>
                    <form action="{{route('subcategory.store')}}" method="post" >
                        @csrf
                        <div class="card-body o-hidden" >
                            <div class="mb-2">
                                <div class="col-form-label">Category title</div>
                                <select name="category_id" class="js-example-placeholder-multiple col-sm-12  @error('category_id') is-invalid @enderror" >
                                    <option selected disabled >Select category</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                                <div class="col-form-label">Subcategory title</div>
                                <div class="d-flex mb-3">
                                    <input autocomplete="off" placeholder="subcategory name" style="margin-right: 20px;"
                                     name="subcategory_name[]" type="text" class="form-control w-85
                                     @error('subcategory_name') is-invalid @enderror ">
                                    @error('subcategory_name')
                                        <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                     <button class="btn" style="background: #0f6e3a;color: white;font-weight:
                                      bold;float: right;margin-left: 20px" id="add_btn">+</button>
                                </div>
                                    <div id="child">

                                    </div>
                                <div class="text-start mt-4">
                                    <input type="submit" value="Add subcategory" class="btn btn-primary">
                                </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>--}}
@push('script')
    <script>
        $(document).ready(function ()
        {
            $('#add_btn').click(function (e)
            {
                e.preventDefault();
                $('#child').append(`
                          <div class="d-flex mb-3">
                             <input autocomplete="off" style="margin-right: 20px;" name="subcategory_name[]" type="text" class="form-control w-85" placeholder="subcategory name">
                            <button id="remove_btn" class="btn btn-danger"  style="background: #0f6e3a;color: white;font-weight: bold;float: right;margin-left: 20px" >x</button>
                          </div>
                       `);

            });

        });
        $(document).on('click','#remove_btn',function (e)
        {
            e.preventDefault();
            let row_item = $(this).parent();
            $(row_item).remove();
        });
    </script>
@endpush
