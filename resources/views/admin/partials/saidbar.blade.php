<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper">
            <a href="{{route('adminpanel')}}">
                <img class="img-fluid for-light" src="{{asset('admin/assets/images/logo/logo.png')}}" alt="">
                <img class="img-fluid for-dark" src="{{asset('admin/assets/images/logo/logo_dark.png')}}" alt="">
            </a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="#"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="#"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="user-check"> </i>
                            Role
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/role/create')|| request()->is('dashboard/role') )? 'd-block' : ''}}">
                            <li>
                                <a style="color: {{request()->is('dashboard/role/create')? 'blue' : ''}}" href="{{route('role.create')}}">
                                    Add role
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('dashboard/role')? 'blue' : ''}}" href="{{route('role.index')}}">
                                    View role
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="layers"> </i>
                            Categories
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/category/create') || request()->is('dashboard/category') ? 'd-block' : '')}} ">
                            <li>
                                <a style="color: {{request()->is('dashboard/category/create')? 'blue' : ''}}" href="{{route('category.create')}}">
                                    Add category
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('dashboard/category')? 'blue' : ''}}" href="{{route('category.index')}}">
                                    View category
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="list"> </i>
                            Subcategories
                        </a>
                        <ul class="sidebar-submenu  {{(request()->is('dashboard/subcategory/create') || request()->is('dashboard/subcategory') ? 'd-block' : '')}} ">
                            <li>
                                <a style="color: {{request()->is('dashboard/subcategory/create')? 'blue' : ''}}" href="{{route('subcategory.create')}}">
                                    Add subcategory
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('dashboard/subcategory')? 'blue' : ''}}" href="{{route('subcategory.index')}}">
                                    View subcategory
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="users"> </i>
                            Members
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/architecture')|| request()->is('dashboard/users') )? 'd-block' : ''}}  ">
                            <li>
                                <a  style="color: {{request()->is('dashboard/architecture') ? 'blue' : ''}}" href="{{route('dashboard.architecture')}}">
                                    View architecture
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('dashboard/users') ? 'blue' : ''}}" href="{{route('dashboard.users')}} ">
                                    View users
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <label class="badge badge-danger">{{$receive_products_count}}</label>
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="box"> </i>
                            Receive 3D
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/receive')|| request()->routeIs('product.show_receive') || request()->is('dashboard/accept')|| request()->is('dashboard/cancel'))? 'd-block' : ''}} ">
                            <li>
                                <a style="color: {{(request()->is('dashboard/receive')||request()->routeIs('product.show_receive') )? 'blue' : ''}}"  href="{{route('receive.product')}}">View receive models</a>
                            </li>

                            <li>
                                <a style="color: {{request()->is('dashboard/accept') ? 'blue' : ''}}" href="{{route('accept.product')}}">View accept model</a>
                            </li>

                            <li>
                                <a style="color: {{request()->is('dashboard/cancel') ? 'blue' : ''}}"  href="{{route('cancel.product')}}">View cancel model</a>
                            </li>
                        </ul>
                    </li>


                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="users"> </i>
                            Receive messages
                        </a>
                        <ul class="sidebar-submenu {{request()->is('dashboard/contact')? 'd-block' : ''}}  ">
                            <li>
                                <a  style="color: {{request()->is('dashboard/contact') ? 'blue' : ''}}" href="{{route('receive.contact')}}">
                                   View receiving messages
                                </a>
                            </li>
{{--                            <li>--}}
{{--                                <a style="color: {{request()->is('dashboard/users') ? 'blue' : ''}}" href="{{route('dashboard.users')}} ">--}}
{{--                                    View users--}}
{{--                                </a>--}}
{{--                            </li>--}}
                        </ul>
                    </li>

                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
