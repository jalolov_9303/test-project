@extends('admin.layouts.master')
@section('title' , 'Categories edit')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3 ">
                    <div class="card-header">
                        <h5 class="card-title">Category update</h5>
                    </div>
                    <form action="{{route('category.update',$category)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Category title</div>
                                <input type="text" value="{{$category->category_name}}" name="category_name" class="form-control @error('category_name') is-invalid @enderror ">
                                @error('category_name')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div>
                                <div class="text-start mt-4">
                                    <input type="submit" value="Update  category" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
