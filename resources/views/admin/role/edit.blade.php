@extends('admin.layouts.master')
@section('title' , 'update role')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3 ">
                    <div class="card-header">
                        <h5 class="card-title">update role to user</h5>
                    </div>
                    <form action="{{route('role.update',$role)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Select user</div>
                                <select name="user_id" class="js-example-placeholder-multiple col-sm-12  @error('user_id') is-invalid @enderror" >
                                    @foreach($users as $user)
                                            <option   value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-2">
                                <div class="col-form-label">Select role</div>
                                <select name="role_id" class="js-example-basic-single col-sm-12 @error('role_id') is-invalid @enderror">
                                    @if($user->role == 1)
                                        <option selected  value="1">Administrator</option>
                                        <option value="2">Arxitektoe</option>
                                        <option value="3">user</option>
                                    @elseif($user->role == 2)
                                        <option value="1">Administrator</option>
                                        <option selected value="2">Arxitektoe</option>
                                        <option value="3">user</option>
                                    @elseif($user->role == 3)
                                        <option value="1">Administrator</option>
                                        <option value="2">Arxitektoe</option>
                                        <option selected value="3">user</option>
                                    @endif
                                </select>
                                @error('role_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div>
                                <div class="text-start mt-2">
                                    <input type="submit" value="update role" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection




