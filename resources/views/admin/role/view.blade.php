@extends('admin.layouts.master')
@section('title' , 'Role view')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-3">Role view </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            №
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            Name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Email
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            Role
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            action
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $key=>$role)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$role->name}}</td>
                                            <td>{{$role->email}}</td>
                                            <td>
                                                @if($role->role_id == 1)
                                                    Administrator
                                                @elseif($role->role_id ==2)
                                                    Arxitektor
                                                @elseif($role->role_id == 3)
                                                    user
                                                @endif
                                            </td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route('role.edit',$role)}}">Edit</a>
                                                </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                    {{--                            <tfoot>--}}
                                    {{--                            <tr>--}}
                                    {{--                                <th rowspan="1" colspan="1">Name</th>--}}
                                    {{--                                <th rowspan="1" colspan="1">Position</th>--}}
                                    {{--                                <th rowspan="1" colspan="1">Office</th>--}}
                                    {{--                                <th rowspan="1" colspan="1">Age</th>--}}
                                    {{--                                <th rowspan="1" colspan="1">Start date</th>--}}
                                    {{--                                <th rowspan="1" colspan="1">Salary</th></tr>--}}
                                    {{--                            </tfoot>--}}
                                </table>
                                {{--                        <div class="dataTables_info" id="advance-1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_simple_numbers" id="advance-1_paginate"><a class="paginate_button previous disabled" aria-controls="advance-1" data-dt-idx="0" tabindex="0" id="advance-1_previous">Previous</a><span><a class="paginate_button current" aria-controls="advance-1" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="advance-1" data-dt-idx="7" tabindex="0" id="advance-1_next">Next</a></div></div>--}}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

@endsection
