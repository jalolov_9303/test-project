@extends('admin.layouts.master')
@section('title' , 'role update')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3 ">
                    <div class="card-header">
                        <h5 class="card-title">Add role to user</h5>
                    </div>
                    <form action="{{route('role.store')}}" method="post">
                        @csrf
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Select user</div>
                                <select name="user_id" class="js-example-placeholder-multiple col-sm-12  @error('user_id') is-invalid @enderror" >
                                    <option selected disabled>Select user</option>
                                    @foreach($users as $user)
                                        @if(old('user_id') == $user->id)
                                            <option selected  value="{{$user->id}}">{{$user->name}}</option>
                                        @else
                                            <option   value="{{$user->id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-2">
                                <div class="col-form-label">Select role</div>
                                <select name="role_id" class="js-example-basic-single col-sm-12 @error('role_id') is-invalid @enderror">
                                    <option selected disabled>select role</option>
                                    @if(old('role_id') == 1)
                                    <option selected value="1">Administrator</option>
                                    @elseif(old('role_id') == 2)
                                    <option selected value="2">Arxitektoe</option>
                                    @elseif(old('role_id') == 3)
                                    <option selected value="3">user</option>
                                    @else
                                        <option value="1">Administrator</option>
                                        <option value="2">Arxitektoe</option>
                                        <option value="3">user</option>
                                    @endif
                                </select>
                                @error('role_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div>
                                <div class="text-start mt-2">
                                    <input type="submit" value="Add role" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
