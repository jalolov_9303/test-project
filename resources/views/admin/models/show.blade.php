@extends('admin.layouts.master')
@section('title','Show product')
@section('content')
    <br>
    <dv>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 mb-xl-0 " >
                      <div class="d-flex align-items-center justify-content-between">
                          <h5 class="">Receive Product show</h5>
                          <h5 class="">By architecture {{$product->user_name->name}}</h5>
                      </div>
                        <div class="demo-inline-spacing mt-4 ">
                            <div class="list-group list-group-flush">
                                <p class="d-flex align-items-center mb-0"> &nbsp;&nbsp; <b>Model name:</b> &nbsp;&nbsp;
                                    <span>{{$product->model_name}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0"> &nbsp;&nbsp; <b>Category:</b> &nbsp;&nbsp;
                                    <span>{{$product->category->category_name}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0">&nbsp;&nbsp; <b>Subcategory:</b> &nbsp;&nbsp;
                                    <span>{{$product->subcategory->subcategory_name}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0"> &nbsp;&nbsp; <b>Model price:</b> &nbsp;&nbsp;
                                    <span>{{$product->model_price}} &nbsp; so'm</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0">&nbsp;&nbsp; <b>Model size:</b> &nbsp;&nbsp;
                                    <span>{{round($product->model_size/1048576 , 2)}} &nbsp; mb</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0"> &nbsp;&nbsp; <b>Model name:</b> &nbsp;&nbsp;
                                    <span>{{$product->model_description}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center mb-0"> &nbsp;&nbsp; <b>Model add time:</b> &nbsp;&nbsp;
                                    <span>{{substr($product->created_at,0,10)}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center  mb-0 "> &nbsp;&nbsp; <b>Model type:</b> &nbsp;&nbsp;
                                    <span>{{$product->model_type}}</span></p>
                                <hr class="m-1 mb-3">
                                <p class="d-flex align-items-center  p-0 mb-0 "> &nbsp;&nbsp; <b>Rate now:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span>
                                        <i data-feather="star"></i>
                                        <i data-feather="star"></i>
                                        <i data-feather="star"></i>
                                        <i data-feather="star"></i>
                                        <i data-feather="star"></i>
                                    </span>
                                </p>
                                <hr class="m-0 p-0 mt-0">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card">
            <div class="row">
                <div class="col-lg-11 " >
                    <div class="mt-3 ">
                        <div class="d-flex  align-items-center   justify-content-between ">
                            <p style="margin-left: 30px">
                                <a href="{{asset('architec/models/files/'.$product->model_type.'/'.$product->model_file)}}" download>
                                    <b style="color: #0a58ca">Download</b> &nbsp;
                                    <b style="color: crimson">Spinner</b>&nbsp;
                                    <b>3D model</b>
                                </a>
                            </p>
                            <p>
                                <a  class="btn btn-danger" href="{{asset('architec/models/files/'.$product->model_type.'/'.$product->model_file)}}" download>Download</a>
                            </p>


                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row mb-5">
            <div class="col-md">
                <div class="card mb-3">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img class="card-img card-img-left" src="{{asset('architec/assets/images/img/images.jpg')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">About <span class="text-success">{{$product->model_name}} </span> 3D model</h5>
                                <p style="color: black" class="card-text ">
                                    {{$product->model_description}}
                                </p>
                                <p class="card-text">
                                    <small class="text-muted"><span class="bold text-info">Created date</span>&nbsp;&nbsp; {{substr($product->created_at,0,10)}}</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @for($i=0;$i<count($model_img_big)-1;$i++)
            <div class="row mb-5">
                <div class="col-md">
                    <div class="card mb-3">
                        <div class="row ">
                            <div class="col-md-6">
                                <img class="card-img card-img-left" src="{{asset('architec/models/images/big/'.$model_img_big[$i])}}" alt="">
                            </div>
                            <div class="col-md-4">
                                <img class="card-img card-img-left" src="{{asset('architec/models/images/medium/'.$model_img_medium[$i])}}" alt="">
                            </div>
                            <div class="col-md-2">
                                <img class="card-img card-img-left" src="{{asset('architec/models/images/small/'.$model_img_small[$i])}}" alt="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
    @endfor
@endsection
