@extends('admin.layouts.master')
@section('title' , 'Receives')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card mt-4">
                    <div class="card-header">
                        <h5 class="mb-3">Receive 3D models</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            №
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                           Architecture
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Model name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Model price
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            View more
                                        </th>

                                        <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            action
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $key=>$product)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$product->user_name->name}}</td>
                                            <td>{{$product->model_name}}</td>
                                            <td>{{$product->model_price}} so'm</td>
                                            <td>
                                                <a href="{{route('product.show_rexceive',$product)}}" class="btn btn-dark">Details</a>
                                            </td>
                                            <td class="d-flex">

                                                <div class="m-t-15 d-flex">
                                                    <a href="{{route('product.press_accept',$product)}}" class="btn btn-success" >
                                                        Accept
                                                    </a>&nbsp;

                                                    <button class="btn btn-danger" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalmdo" data-whatever="@fat" data-bs-original-title="" title="">
                                                        Cancel
                                                    </button>
                                                    <div class="modal fade" id="exampleModalmdo" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Do you <span class="btn btn-danger">cancel</span> description to user</h5>
                                                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                                                </div>
                                                                <form action="{{route('product.reason',$product)}}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="mb-3">
                                                                            <label class="col-form-label" for="message-text">Message:</label>
                                                                            <textarea id="message-text" name="status_reason" class="form-control"></textarea>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                                                            <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">Send message</button>
                                                                        </div>

                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach

                                </table>
                                {{--                        <div class="dataTables_info" id="advance-1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_simple_numbers" id="advance-1_paginate"><a class="paginate_button previous disabled" aria-controls="advance-1" data-dt-idx="0" tabindex="0" id="advance-1_previous">Previous</a><span><a class="paginate_button current" aria-controls="advance-1" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="advance-1" data-dt-idx="7" tabindex="0" id="advance-1_next">Next</a></div></div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
        @push('script')
            <script>
                $(document).ready(function ()
                {
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $("#check_status").click(function (){
                        if ($(this).is(':checked'))
                        {
                            console.log('yes');
x
                        }else {
                            console.log('no');
                        }
                        var category_id = $(this).val();
                        {{--$.ajax({--}}
                        {{--    url:'{{route('architec.category')}}',--}}
                        {{--    method: 'POST',--}}
                        {{--    data:{--}}
                        {{--        'category_id' : category_id--}}
                        {{--    } ,--}}
                        {{--    success:function (response){--}}
                        {{--        for (i=0; i<response.subcategories.length; i++)--}}
                        {{--        {--}}
                        {{--            $("#subcategory_id").append('<option value='+response.subcategories[i].id+' >'+response.subcategories[i].subcategory_name+'</option>');--}}
                        {{--        }--}}
                        {{--    },--}}
                        {{--    error:function (response)--}}
                        {{--    {--}}
                        {{--        console.log(response)--}}
                        {{--    }--}}
                        {{--});--}}
                    });

                    //subcategory

                    {{--$("#subcategory_id").change(function (){--}}
                    {{--    $("#category_id").html('');--}}
                    {{--    var subcategory_id = $(this).val();--}}
                    {{--    $.ajax({--}}
                    {{--        url:'{{route('architec.subcategory')}}',--}}
                    {{--        method: 'POST',--}}
                    {{--        data:{--}}
                    {{--            'subcategory_id' : subcategory_id--}}
                    {{--        } ,--}}
                    {{--        success:function (response){--}}
                    {{--            // console.log(response.categories);--}}
                    {{--            // console.log(response.category_id.category_id);--}}
                    {{--            for (i=0; i<response.categories.length; i++)--}}
                    {{--            {--}}
                    {{--                if (response.category_id.category_id === response.categories[i].id)--}}
                    {{--                {--}}
                    {{--                    $("#category_id").append('<option selected value='+response.categories[i].id+' >'+response.categories[i].category_name+'</option>');--}}
                    {{--                }--}}
                    {{--                else {--}}
                    {{--                    $("#category_id").append('<option  value='+response.categories[i].id+' >'+response.categories[i].category_name+'</option>');--}}

                    {{--                }--}}
                    {{--            }--}}
                    {{--        },--}}
                    {{--        error:function (response)--}}
                    {{--        {--}}
                    {{--            console.log(response)--}}
                    {{--        }--}}
                    {{--    });--}}
                    {{--});--}}
                });
            </script>
    @endpush
