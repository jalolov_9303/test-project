@extends('admin.layouts.master')
@section('title' , 'Receives')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card mt-4">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h5 class="mb-3">Receive 3D models</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            №
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Architecture
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Model name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Model price
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            View more
                                        </th>

                                        <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            action
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $key=>$product)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$product->user_name->name}}</td>
                                            <td>{{$product->model_name}}</td>
                                            <td>{{$product->model_price}} so'm</td>
                                            <td>
                                                <a href="{{route('product.show_receive',$product)}}" class="btn btn-dark">Details</a>
                                            </td>

                                           <td>
                                               <div class="col-sm-8 col-6">
                                                   <div class="media-body text-start icon-state">
                                                       <label class="switch">
                                                           @if($product->block ==1 && $product->status ==1)
                                                           <input checked class="check_status" name="check_status" data-product-id="{{$product->id}}"
                                                                  value="1" id="check_status" type="checkbox"  >
                                                               <span class="switch-state"></span>

                                                           @elseif($product->block ==1 && $product->status ==0)
                                                               <input class="check_status" checked data-product-id="{{$product->id}}" value=0" id="check_status"
                                                                      type="checkbox" data-bs-original-title="" title="">
                                                               <span class="switch-state"></span>

                                                           @endif
                                                       </label>
                                                   </div>
                                               </div>
                                           </td>

                                        </tr>
                                    @endforeach

                                </table>
                                {{--                        <div class="dataTables_info" id="advance-1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_simple_numbers" id="advance-1_paginate"><a class="paginate_button previous disabled" aria-controls="advance-1" data-dt-idx="0" tabindex="0" id="advance-1_previous">Previous</a><span><a class="paginate_button current" aria-controls="advance-1" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="advance-1" data-dt-idx="7" tabindex="0" id="advance-1_next">Next</a></div></div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
        @push('script')
            <script>
                $(document).ready(function ()
                {
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $(".check_status").change(function () {
                        var status = 0;
                        if (this.checked) {
                            status = 1;

                        } else {
                            status = 0;
                        }
                        let product_id = $(this).attr('data-product-id');
                        $.ajax({
                            url: '{{route('product.block')}}',
                            method: 'POST',
                            data: {
                                'status': status,
                                'product_id': product_id
                            },
                            success: function (response) {

                            }
                        });

                    });

                });
            </script>
    @endpush
