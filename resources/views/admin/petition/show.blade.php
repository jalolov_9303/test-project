@extends('admin.layouts.master')
@section('title' , 'Show petition')
@section('content')
    <br>
    <br>
    <div class="col-xl-12 xl-100 ">
        <div class="card ">
            <div class="card-body mt-3">
                <div class="product-page-details">
                    <h3>Notification {{$petition->user_name->name}}</h3>
                </div>

                <ul class="product-color mt-2">
                    <li class="bg-primary"></li>
                    <li class="bg-secondary"></li>
                    <li class="bg-success"></li>
                    <li class="bg-info"></li>
                    <li class="bg-warning"></li>
                </ul>
                <hr>

                <div>
                    <table class="product-page-width">
                        <tbody>
                        <tr>
                            <td>
                                <b>Name &nbsp;&nbsp;&nbsp;:</b>
                            </td>
                            <td>{{$petition->user_name->name}}</td>
                        </tr>
                        <tr>
                            <td>
                                <b>Email &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b>
                            </td>
                            <td style="color: #0a53be">{{$petition->user_name->email}}</td>
                        </tr>
                        <tr>
                            <td>
                                <b>Petition text &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b>
                            </td>
                            <td >{{$petition->petition_text}}</td>
                        </tr>
                        <tr>
                            <td>
                                <b>Portfolio link &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b>
                            </td>
                            <td>{{$petition->portfolio_link}} ...</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <hr>

                <div class="m-t-15">
                    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalmdoaccept" data-whatever="@fat" data-bs-original-title="" title="">
                        Accept
                    </button>
                    <div class="modal fade" id="exampleModalmdoaccept" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Do you <span class="btn btn-primary">accept</span> for this user ?</h5>
                                </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                            <a href="{{route('dashboard.notification.accept',$petition)}}" class="btn btn-primary" type="button" data-bs-original-title="" title="">Send message</a>
                                        </div>


                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalmdo" data-whatever="@fat" data-bs-original-title="" title="">
                        Cancel
                    </button>
                    <div class="modal fade" id="exampleModalmdo" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Do you <span class="btn btn-danger">cancel</span> description to user</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                </div>
                                <form action="{{route('dashboard.notification.cancel',$petition)}}" method="post">
                                    @csrf
                                       <div class="modal-body">
                                        <div class="mb-3">
                                            <label class="col-form-label" for="message-text">Message:</label>
                                            <textarea name="result_description" class="form-control"></textarea>
                                        </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                    <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">Send message</button>
                                </div>

                            </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
