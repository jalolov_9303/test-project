@extends('admin.layouts.master')
@section('title' , 'users')
@section('content')
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h5 class="mb-3">In your platform registrated <span class="btn btn-success">{{$users_count}}</span> users</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            №
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            Users name
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            users email
                                        </th>
                                        <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Block
                                        </th>
                                        <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Rating
                                        </th>

                                        <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            action
                                        </th>

                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr >
                                            <td>{{$key+1}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td class="text-center"><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i></td>
                                            <td >
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                @if($user->spam_status == 0)
                                                    <button class="btn btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#exampleModalmdo" data-whatever="@fat" data-bs-original-title="" title="">
                                                        No spam
                                                    </button>
                                                    <div class="modal fade" id="exampleModalmdo" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Do you <span class="btn btn-danger">Spam</span> description to Architecture</h5>
                                                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                                                </div>
                                                                <form action="{{route('user.spam',$user)}}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="mb-3">
                                                                            <label class="col-form-label" for="message-text">Message:</label>
                                                                            <textarea id="message-text" name="status_reason" class="form-control"></textarea>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                                                            <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">Send message</button>
                                                                        </div>

                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($user->spam_status == 1)
                                                    <a href="{{route('user.nospam',$user)}}" class="btn btn-danger">
                                                        Spam
                                                    </a>
                                                @endif
                                            </td>

                                            <td class="d-flex">
                                                <a class="btn btn-primary" href="">Edit</a> &nbsp;
                                                <form action="">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
