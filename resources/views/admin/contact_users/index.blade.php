@extends('admin.layouts.master')
@section('title','Contact')
@section('content')
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h5 class="mb-3">All contact messages</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            №
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                            User name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            User email
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                            Message
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                            Receiving date
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contacts as $key=>$contact)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td>{{$contact->text}}</td>
                                            <td>{{$contact->created_at}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
