@extends('architec.layouts.master')
@section('title' , 'Product view')
@section('content')
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card mt-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-3">Product view </h5>
                            <a class="btn btn-primary" href="{{route('product.create')}}">Model create</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="advance-1_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="advance-1_length"><label>Show <select name="advance-1_length" aria-controls="advance-1" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="advance-1_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="advance-1"></label></div><table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171.3px;">
                                                №
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                                Model name
                                            </th>

                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                                Category
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                                Model price
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                                Status
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 280.5px;">
                                                status_reason
                                            </th>

                                            <th class="sorting text-center" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 116.312px;">
                                                action
                                            </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $key=>$product)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$product->model_name}}</td>
                                                <td>{{$product->category_name}}</td>
                                                <td>{{$product->model_price}} so'm</td>
                                                <td>
                                                    @if($product->status ==0)
                                                        <span class="badge badge-warning">Checking</span>
                                                    @elseif($product->status ==1)
                                                        <span class="badge badge-success">Accept</span>
                                                    @elseif($product->status ==2)
                                                        <span class="badge badge-danger">Cancel</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button class="btn btn-dark">reason</button>
                                                </td>
                                                <td class="d-flex">
                                                    <a class="btn btn-primary" href="{{route('product.show',$product)}}">Detail</a>&nbsp;
                                                    <a class="btn btn-success" href="{{route('product.edit',$product)}}">Edit</a>&nbsp;

                                                </td>
                                            </tr>
                                        @endforeach

                                    </table>
                                    {{--                        <div class="dataTables_info" id="advance-1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_simple_numbers" id="advance-1_paginate"><a class="paginate_button previous disabled" aria-controls="advance-1" data-dt-idx="0" tabindex="0" id="advance-1_previous">Previous</a><span><a class="paginate_button current" aria-controls="advance-1" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="advance-1" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="advance-1" data-dt-idx="7" tabindex="0" id="advance-1_next">Next</a></div></div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
