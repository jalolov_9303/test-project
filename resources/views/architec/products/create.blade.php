@extends('architec.layouts.master')
@section('title','Create product')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3 ">
                    <div class="card-header">
                        <h5 class="card-title">Create product 3D model</h5>
                    </div>
                    <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                             <div class="card-body o-hidden">
                               <div class="mb-3">
                                <div class="col-form-label">Model name</div>
                                <input autocomplete="off" value="{{old('model_name')}}" type="text" name="model_name" placeholder="Model name" class="form-control @error('model_name') is-invalid @enderror ">
                                    @error('model_name')
                                        <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="mb-3">
                                <label for="category_id" class="form-label">Select category</label>
                                <select class="form-select @error('category_id') is-invalid @enderror"  aria-label="Default select example" name="category_id" id="category_id">
                                    <option selected value="">Select category</option>
                                    @foreach($categories as $category)
                                         <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="subcategory_id" class="form-label">Select subcategory</label>
                                <select name="subcategory_id" class="form-select @error('subcategory_id') is-invalid @enderror" id="subcategory_id" aria-label="Default select example">
                                    <option selected="" value="">Select subcategory</option>
                                    @foreach($subcategories as $subcategory)
                                        <option value="{{$subcategory->id}}">{{$subcategory->subcategory_name}}</option>
                                    @endforeach
                                </select>
                                @error('subcategory_id')
                                <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlSelect1" class="form-label">Model price (so'm)</label>
                                <input value="{{old('model_price')}}" type="number" name="model_price" placeholder="Price" class="form-control @error('model_price') is-invalid @enderror ">
                                    @error('model_price')
                                        <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                         </span>
                                    @enderror
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlSelect1" class="form-label">Model images</label>
                                <input value="{{old('model_img')}}" multiple type="file" name="model_img[]"  class="form-control @error('model_img') is-invalid @enderror ">
                                @error('model_img')
                                <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                             </span>
                                @enderror
                                @foreach($errors->get('model_img.*') as $error)
                                    <div class="alert alert-danger">{{$error[0]}}</div>
                                @endforeach
                            </div>


                            <div class="mb-3">
                                <label for="exampleFormControlSelect1" class="form-label">Model file (.max, .blend, .obj, .fbx, .revit)</label>
                                <input value="{{old('model_file')}}" type="file" name="model_file" placeholder="Price" class="form-control @error('model_file') is-invalid @enderror ">
                                @error('model_file')
                                <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                             </span>
                                @enderror
                            </div>


                            <div class="col mb-3">
                                <div>
                                    <label class="form-label" for="exampleFormControlTextarea4">Model description</label>
                                    <textarea  name="model_description" class="form-control @error('model_description') is-invalid @enderror " id="exampleFormControlTextarea4" rows="3">
                                        {{old('model_description')}}

                                    </textarea>

                                    @error('model_description')
                                    <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                             </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="text-start mt-4">
                                <input type="submit" value="Create product" class="btn btn-primary">
                            </div>

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function ()
        {
            $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    }
                });

            $("#category_id").change(function (){
                $("#subcategory_id").html('');
                var category_id = $(this).val();
                $("#subcategory_id").append('<option value="null" selected disabled>{{ __('Select category')}}</option>');
                $.ajax({
                   url:'{{route('architec.category')}}',
                   method: 'POST',
                   data:{
                       'category_id' : category_id
                   } ,
                    success:function (response){
                        for (i=0; i<response.subcategories.length; i++)
                        {
                            $("#subcategory_id").append('<option value='+response.subcategories[i].id+' >'+response.subcategories[i].subcategory_name+'</option>');
                        }
                    },
                    error:function (response)
                    {
                        console.log(response)
                    }
                });
            });

            //subcategory

            $("#subcategory_id").change(function (){
                $("#category_id").html('');
                var subcategory_id = $(this).val();
                $.ajax({
                    url:'{{route('architec.subcategory')}}',
                    method: 'POST',
                    data:{
                        'subcategory_id' : subcategory_id
                    } ,
                    success:function (response){
                        // console.log(response.categories);
                        // console.log(response.category_id.category_id);
                        for (i=0; i<response.categories.length; i++)
                        {
                            if (response.category_id.category_id === response.categories[i].id)
                            {
                                $("#category_id").append('<option selected value='+response.categories[i].id+' >'+response.categories[i].category_name+'</option>');
                            }
                            else {
                                $("#category_id").append('<option  value='+response.categories[i].id+' >'+response.categories[i].category_name+'</option>');

                            }
                        }
                    },
                    error:function (response)
                    {
                        console.log(response)
                    }
                });
            });
        });
    </script>
@endpush
