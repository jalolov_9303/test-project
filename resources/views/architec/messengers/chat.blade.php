@extends('architec.layouts.master')
@section('title' , 'messengers')
@section('content')
    <div class="row">
        <div class="col call-chat-sidebar col-sm-12">
            <div class="card mt-4">
                <div class="card-body chat-body">
                    <div class="chat-box">
                        <!-- Chat left side Start-->
                        <div class="chat-left-aside">
                            <div class="media">
                                <img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                <div class="about">

                                    <div class="name f-w-600">{{\Illuminate\Support\Facades\Auth::user()->name}}</div>

                                    <div class="status">Status...</div>
                                </div>
                            </div>
                            <div class="people-list" id="people-list">
                                <div class="search">
                                    <div class="mb-3">
                                        <input id="search" class="form-control" type="text" placeholder="Search" data-bs-original-title="" title=""><i class="fa fa-search"></i>
                                    </div>
                                </div>
                                <ul class="list" id="members_list">
                                    @foreach($admin_names as $admin_name)
                                        <li class="clearfix">
{{--                                            <a href="{{route('messengers.chat',$member)}}">--}}
                                                <img class="rounded-circle user-image"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                                <div class="status-circle away"></div>
                                                <div class="about">
                                                    <div class="name">{{$admin_name->name}}</div>
                                                    <div class="status">
                                                        Administrator
                                                    </div>
                                                </div>
{{--                                            </a>--}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- Chat left side Ends-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col call-chat-body">
            <div class="card mt-4">
                <div class="card-body p-0">
                    <div class="row chat-box">
                        <!-- Chat right side start-->
                        <div class="col pe-0 chat-right-aside">
                            <!-- chat start-->
                            <div class="chat">
                                <!-- chat-header start-->
                                <div class="chat-header clearfix"><img class="rounded-circle"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                    <div class="about">
                                        <div class="name">Administrator <span class="font-primary f-12">Typing...</span></div>
                                        <div class="status">Last Seen 3:55 PM</div>
                                    </div>
                                    <ul class="list-inline float-start float-sm-end chat-menu-icons">
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-search"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-clip"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-headphone-alt"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-video-camera"></i></a></li>
                                        <li class="list-inline-item toogle-bar"><a href="#" data-bs-original-title="" title=""><i class="icon-menu"></i></a></li>
                                    </ul>
                                </div>
                                <!-- chat-header end-->
                                <div class="chat-history chat-msg-box custom-scrollbar">
                                    <ul>
                                        @foreach($all_messengers_with_admin as $all_messenger_with_admin)
                                            @if($all_messenger_with_admin->admin_messege == null)
                                                <li class="clearfix">
                                                    <div class="message other-message pull-right">
                                                        <img class="rounded-circle float-end chat-user-img img-30"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                                        {{$all_messenger_with_admin->architec_messege}}
                                                        <div class="message-data"><span class="message-data-time">{{substr($all_messenger_with_admin->created_at,11,17)}}</span></div>
                                                    </div>
                                                </li>

                                            @elseif($all_messenger_with_admin->architec_messege == null)
                                                <li>
                                                    <div class="message my-message"><img class="rounded-circle float-start chat-user-img img-30"  src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                                        {{$all_messenger_with_admin->admin_messege}}
                                                        <div class="message-data text-end"><span class="message-data-time">{{substr($all_messenger_with_admin->created_at,11,17)}}</span></div>
                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- end chat-history-->
                                <div class="chat-message clearfix">
                                    <div class="row">
                                            <form action="{{route('architec_text_message')}}" method="post">
                                            @csrf
                                            <div class="col-xl-12 d-flex">
                                                <div class="input-group text-box">
                                                    <input autocomplete="off" class="form-control  @error('architec_message') is-invalid @enderror" id="message-to-send" type="text" name="architec_message" placeholder="Type a message......" data-bs-original-title="" title="">
                                                    <button class="btn-primary" type="submit" data-bs-original-title="" title="">Send</button>
                                                </div>

                                                @error('architec_message')
                                                <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                                @enderror

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
