<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>Riode</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Riode - eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('main/images/icons/favicon.png')}}">
    <!-- Preload Font -->
    <link rel="preload" href="{{asset('main/fonts/riode.ttf?5gap68')}}" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{asset('main/vendor/fontawesome-free/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="{{asset('main/vendor/fontawesome-free/webfonts/fa-brands-400.woff2')}}" as="font" type="font/woff2"
          crossorigin="anonymous">
    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:400,500,600,700,800' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/animate/animate.min.css')}}">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/sticky-icon/stickyicon.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('main/css/demo29.min.css')}}">
</head>

<body>

<div class="page-wrapper">
    <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
    @include('main.partials.saidbar')
    <!-- End Header -->
    <main class="main home mt-4">
        <div class="page-content">
            <section class="intro-slider animation-slider owl-carousel owl-theme row cols-1 gutter-no"
                     data-owl-options="{
                    'items': 1,
                    'loop': true,
                    'nav': false,
                    'dots': false,
                    'responsive': {
                        '992': {
                            'nav': true
                        }
                    }
                }">
                <div class="intro-slide1 banner banner-fixed" style="background-color: #e6e8e9;">
                    <figure>
                        <img src="{{asset('main/images/demos/demo29/slides/1.jpg')}}" alt="banner" width="1903" height="469" />
                    </figure>
                    <div class="container">
                        <div class="banner-content y-50">
                            <h3 class="banner-subtitle d-inline-block slide-animate text-uppercase"
                                data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'duration': '1.5s',
                                'delay': '.3s'
                            }">From Online Store</h3>
                            <h2 class="banner-title slide-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'duration': '1.3s',
                                'delay': '.4s'
                            }">Originals Comper Star. Xbox</h2>
                            <p class="font-primary mb-6 slide-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'duration': '1.2s',
                                'delay': '.5s'
                            }">Free Shipping on all orders over $320.00</p>
                            <a href="demo29-shop.html"
                               class="btn btn-rounded btn-dark btn-outline mb-1 slide-animate"
                               data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'duration': '1s',
                                'delay': '.6s'
                            }">Shop Now<i class="d-icon-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="intro-slide2 banner banner-fixed" style="background-color: #e6e8e9;">
                    <figure>
                        <img src="{{asset('main/images/demos/demo29/slides/2.jpg')}}" alt="banner" width="1903" height="469" />
                    </figure>
                    <div class="container">
                        <div class="banner-content y-50 ml-auto text-right">
                            <div class="slide-animate" data-animation-options="{
                                    'name': 'blurIn',
                                    'duration': '.8s'
                                }">
                                <h4 class="banner-subtitle text-uppercase">Lifestyle Collection</h4>
                                <h2 class="banner-title"><b class="text-primary">Sophisticated</b>Domestic Electric
                                    Machines</h2>
                                <a href="demo29-shop.html" class="btn btn-rounded btn-outline btn-dark">Shop now<i
                                        class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <!-- End of Top Banner -->
                <section class="banners-grid pt-10 mt-6 pb-6">
                    <h2 class="title title-simple">Special Offers</h2>
                    <div class="row grid">
                        <div class="grid-item col-lg-6 height-x2">
                            <div class="banner banner1 banner-fixed overlay-light appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInRightShorter'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/2.jpg')}}" alt="banner image" width="680"
                                         height="508">
                                </figure>
                                <div class="banner-content top w-100 text-center">
                                    <h4 class="banner-subtitle text-uppercase mb-2">
                                        Deals and Promotion</h4>
                                    <h3 class="banner-title font-weight-bold text-uppercase">Camera &amp; Lens</h3>
                                    <h5 class="text-uppercase">Start at <span class="text-primary">$250.00</span>
                                    </h5>
                                </div>
                                <div class="banner-content bottom w-100 text-center">
                                    <a href="demo29-shop.html"
                                       class="btn btn-rounded btn-outline btn-dark mb-4">Shop Now<i
                                            class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-lg-3 col-xs-6 height-x1">
                            <div class="banner banner2 banner-fixed overlay-light content-middle appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInDownShorter'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/3.jpg')}}" alt="banner image" width="280"
                                         height="207">
                                </figure>
                                <div class="banner-content">
                                    <h5 class="ls-l mb-1 opacity-8">Featured Event</h5>
                                    <h4 class="banner-subtitle text-uppercase ls-s mb-0">Black Friday</h4>
                                    <h3 class="banner-title text-primary font-weight-bold ls-s">Sale</h3>
                                    <a href="demo29-shop.html" class="btn btn-link btn-underline btn-dark">Shop
                                        Now<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-lg-3 col-xs-6 height-x2">
                            <div class="banner banner4 banner-fixed overlay-dark appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInLeftShorter'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/5.jpg')}}" alt="banner image" width="280"
                                         height="434">
                                </figure>
                                <div class="banner-content top w-100 text-center">
                                    <p class="banner-date text-right text-white font-weight-bold">
                                        18-25<sup>TH</sup>MAY</p>
                                    <h5 class="banner-subtitle text-uppercase text-white">
                                        The Season To Play</h5>
                                    <h3 class="banner-title font-weight-bold text-white text-uppercase">Up To 50%
                                    </h3>
                                    <h4 class="text-white text-uppercase font-weight-normal">Xbox one wireless
                                        controller</h4>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-lg-3 col-xs-6 height-x1">
                            <div class="banner banner3 banner-fixed overlay-dark content-middle appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInUpShorter'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/4.jpg')}}" alt="banner image" width="280"
                                         height="207">
                                </figure>
                                <div class="banner-content">
                                    <h5 class="ls-l text-white mb-1 opacity-8">Best Seller</h5>
                                    <h4 class="banner-subtitle text-uppercase ls-s text-white mb-1">Electronic</h4>
                                    <h3 class="banner-title font-weight-bold text-white ls-s ">20% Off</h3>
                                    <a href="demo29-shop.html" class="btn btn-link btn-underline btn-white">Shop
                                        Now<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="grid-space col-lg-3 col-xs-6"></div>
                    </div>
                </section>
            </div>
            <section class="grey-section product-wrapper mt-10 pt-10 pb-4">
                <div class="container pt-2">
                    <div
                        class="title-wrapper with-filters d-flex align-items-center justify-content-between pt-2 mb-4">
                        <h2 class="title title-simple mb-md-0 appear-animate"
                            data-animation-options="{'name': 'fadeInLeftShorter','delay': '.2s'}">New Arrivals</h2>
                        <ul class="nav-filters product-filters font-weight-semi-bold mr-0 appear-animate"
                            data-animation-options="{'name': 'fadeInRightShorter','delay': '.2s'}"
                            data-target="#products-1">
                            <li><a href="#" class="nav-filter active" data-filter="*">All</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".headphones">Headphones</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".accessories">Accessories</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".speakers">Speakers</a></li>
                        </ul>
                    </div>
                    <div id="products-1" class="row grid products-grid pb-2 mb-8 appear-animate" data-grid-options="{
                            'layoutMode': 'fitRows'
                        }">
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 accessories speakers">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/1.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Apple Laptop</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 speakers">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/2.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Bluetooth Speaker</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 accessories headphones speakers">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/3.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Galaxy Tablet 10inchi</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 headphones accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/4.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">SAMSUNG SmartPhone</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 speakers">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/5.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Bluetooth Speaker</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 speakers headphones accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/6.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Apple Sports Watch</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="title-wrapper with-filters d-flex align-items-center justify-content-between pt-1 mb-4">
                        <h2 class="title title-simple mb-md-0 appear-animate"
                            data-animation-options="{'name': 'fadeInLeftShorter','delay': '.2s'}">Best Sellers</h2>
                        <ul class="nav-filters product-filters font-weight-semi-bold mr-0 appear-animate"
                            data-animation-options="{'name': 'fadeInRightShorter','delay': '.2s'}"
                            data-target="#products-2">
                            <li><a href="#" class="nav-filter active" data-filter="*">All</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".headphones">Headphones</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".accessories">Accessories</a></li>
                            <li><a href="#" class="nav-filter" data-filter=".speakers">Speakers</a></li>
                        </ul>
                    </div>
                    <div id="products-2" class="row grid products-grid mb-10 appear-animate" data-grid-options="{
                            'layoutMode': 'fitRows'
                        }">
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 headphones accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/7.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Classic Radio</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/8.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">HP Laptop</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 speakers accessories headphones">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/9.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Apple Watch</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 headphones speakers accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/10.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Apple Laptop</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 accessories">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/11.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Bluetooth Speaker</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item col-xl-2 col-lg-3 col-sm-4 col-6 speakers">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/12.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Add to wishlist"><i
                                                class="d-icon-compare"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">SAMSUNG Smart Phone</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="banner-group pt-10 mt-6 pb-2 mb-10">
                <div class="container">
                    <div class="owl-carousel owl-theme row cols-md-3 cols-sm-2 mb-4" data-owl-options="{
                            'items': 1,
                            'margin': 20,
                            'responsive': {
                                '576': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                }
                            }
                        }">
                        <div class="category category-banner category-absolute category-group-icon text-white chevron-inherit appear-animate"
                             data-animation-options="{
                                'name': 'fadeInLeftShorter',
                                'delay': '.3s'
                            }" style="background: #2C3642;">
                            <figure class="category-media">
                                <img src="{{asset('main/images/demos/demo29/banner/6.jpg')}}" alt="banner image" width="380"
                                     height="213">
                            </figure>
                            <div class="category-content">
                                <h4>Headphones</h4>
                                <ul class="category-list">
                                    <li><a href="demo29-shop.html">Samsung</a></li>
                                    <li><a href="demo29-shop.html">American Dreams</a></li>
                                    <li><a href="demo29-shop.html">Apple</a></li>
                                    <li><a href="demo29-shop.html">Acer</a></li>
                                    <li><a href="demo29-shop.html">Arcade 1 UP</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="category category-banner category-absolute category-group-icon appear-animate"
                             data-animation-options="{
                                'name': 'fadeIn',
                                'delay': '.3s'
                            }" style="background: #e6e6e6;">
                            <figure class="category-media">
                                <img src="{{asset('main/images/demos/demo29/banner/7.jpg')}}" alt="banner image" width="380"
                                     height="213">
                            </figure>
                            <div class="category-content">
                                <h4>Cameras</h4>
                                <ul class="category-list">
                                    <li><a href="demo29-shop.html">Samsung</a></li>
                                    <li><a href="demo29-shop.html">Nikon D850</a></li>
                                    <li><a href="demo29-shop.html">Sony</a></li>
                                    <li><a href="demo29-shop.html">Canon EOS 80D</a></li>
                                    <li><a href="demo29-shop.html">Lumix</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="category category-banner category-absolute category-group-icon text-white appear-animate"
                             data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.3s'
                            }" style="background: #6a6c71;">
                            <figure class="category-media">
                                <img src="{{asset('main/images/demos/demo29/banner/8.jpg')}}" alt="banner image" width="380"
                                     height="213">
                            </figure>
                            <div class="category-content">
                                <h4>Watches</h4>
                                <ul class="category-list">
                                    <li><a href="demo29-shop.html">Dell</a></li>
                                    <li><a href="demo29-shop.html">Lenovo</a></li>
                                    <li><a href="demo29-shop.html">Apple</a></li>
                                    <li><a href="demo29-shop.html">Sony</a></li>
                                    <li><a href="demo29-shop.html">Samsung</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <div class="banner banner5 banner-fixed overlay-light content-middle appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.4s'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/9.jpg')}}" alt="banner image" width="280"
                                         height="207">
                                </figure>
                                <div class="banner-content pl-8">
                                    <h4 class="banner-subtitle text-white font-weight-bold">Flash Sale <span
                                            class="text-primary">50%
                                                OFF</span>
                                    </h4>
                                    <h3 class="banner-title text-white font-weight-bold">Wireless Earphone
                                    </h3>
                                    <p class="text-white font-weight-normal">
                                        Only until the end of this Week</p>
                                    <a href="demo29-shop.html" class="btn btn-rounded btn-white btn-outline">Shop
                                        Now<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="banner banner6 banner-fixed overlay-light content-middle appear-animate"
                                 data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.6s'
                                }">
                                <figure>
                                    <img src="{{asset('main/images/demos/demo29/banner/10.jpg')}}" alt="banner image" width="280"
                                         height="207">
                                </figure>
                                <div class="banner-content pl-8">
                                    <h4 class="banner-subtitle text-white font-weight-normal mb-0">Best Sellers
                                        Store</h4>
                                    <h3 class="banner-title text-white font-weight-bold">Up To 30% OFF</h3>
                                    <p class="text-white font-weight-normal opacity-8">
                                        Feel the charm in this spot</p>
                                    <a href="demo29-shop.html" class="btn btn-rounded btn-white btn-outline">Shop
                                        Now<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="grey-section pt-10 pb-10">
                <div class="container pt-6 mb-4">
                    <h2 class="title text-center d-block appear-animate"
                        data-animation-options="{'name': 'fadeIn', 'delay': '.3s'}">Our
                        Featured</h2>
                    <div class="row mb-10 pb-4 appear-animate">
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/13.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Virtual 3D Glasses</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/14.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Tablet 8inchi</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm product-variable">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/15.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Electronics Waffle Iron</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00-$21.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="demo29-product.html" class="btn-product btn-cart"
                                       title="Select Options"><span>Select
                                                Options</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/16.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Tiny USB Camera</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/17.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Apple Sports Watch</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/18.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Virtual 3D Glasses</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/14.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Tablet 8inchi</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                       data-target="#addCartModal" title="Add to cart"><i
                                            class="d-icon-bag"></i><span>add to cart</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 col-xxl-3">
                            <div class="product product-list-sm product-variable">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/15.jpg')}}" alt="product" width="162"
                                             height="191">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Electronics Waffle Iron</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$19.00-$21.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top">5.00</span>
                                        </div>
                                    </div>
                                    <a href="demo29-product.html" class="btn-product btn-cart"
                                       title="Select Options"><span>Select
                                                Options</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row product-wrapper">
                        <h2 class="title text-center d-block appear-animate"
                            data-animation-options="{'name': 'fadeInUpShorter','delay': '.5s'}">Best of the Week
                        </h2>
                        <div class="owl-carousel owl-theme cols-md-6 cols-4 gutter-sm mb-4 appear-animate"
                             data-owl-options="{
                                'items': 2,
                                'margin': 20,
                                'dots': false,
                                'responsive': {
                                    '480': {
                                        'items': 2
                                    },
                                    '576': {
                                        'items': 3
                                    },
                                    '768': {
                                        'items': 4
                                    },
                                    '992': {
                                        'items': 6
                                    }
                                }
                            }">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/19.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">XP Digital Camera</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/20.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Canon Digital Camera 16x</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/21.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Sony Digital Camera</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/22.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Fashion Headphone</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/23.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Headphone</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="demo29-product.html">
                                        <img src="{{asset('main/images/demos/demo29/products/24.jpg')}}" alt="product" width="168"
                                             height="190">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                           data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo29-shop.html">categories</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="demo29-product.html">Headphone</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="demo29-product.html" class="rating-reviews">( 2 )</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="blog-section pt-10 mt-3 pb-8">
                <div class="container">
                    <h2 class="title text-center d-block">Latest News</h2>
                    <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2 cols-1" data-owl-options="{
                            'items': 1,
                            'margin': 20,
                            'loop': false,
                            'responsive': {
                                '576': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                },
                                '992': {
                                    'items': 4
                                }
                            }
                        }">
                        <div class="post overlay-zoom overlay-dark appear-animate"
                             data-animation-options="{'name': 'fadeInRightShorter', 'delay': '.1s'}">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{asset('main/images/demos/demo29/blog/1.jpg')}}" width="380" height="250" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    <a href="#" class="post-date"><span>on</span>Nov 22, 2018</a> | <a href="#"
                                                                                                       class="post-comment"><mark>0</mark> Comments</a>
                                </div>
                                <h3 class="post-title"><a href="#">Sed adipiscing ornare.</a></h3>
                                <a href="blog-classic.html"
                                   class="btn btn-link btn-underline ls-normal text-normal font-weight-normal">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-zoom overlay-dark appear-animate"
                             data-animation-options="{'name': 'fadeInRightShorter', 'delay': '.2s'}">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{asset('main/images/demos/demo29/blog/2.jpg')}}" width="380" height="250" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    <a href="#" class="post-date"><span>on</span>Nov 22, 2018</a> | <a href="#"
                                                                                                       class="post-comment"><mark>0</mark> Comments</a>
                                </div>
                                <h3 class="post-title"><a href="#">Aenean dignissim pellentesque.</a></h3>
                                <a href="blog-classic.html"
                                   class="btn btn-link btn-underline ls-normal text-normal font-weight-normal">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-zoom overlay-dark appear-animate"
                             data-animation-options="{'name': 'fadeInRightShorter', 'delay': '.3s'}">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{asset('main/images/demos/demo29/blog/3.jpg')}}" width="380" height="250" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    <a href="#" class="post-date"><span>on</span>Nov 22, 2018</a> | <a href="#"
                                                                                                       class="post-comment"><mark>0</mark> Comments</a>
                                </div>
                                <h3 class="post-title"><a href="#">Quisque volutpat mattis eros.</a></h3>
                                <a href="blog-classic.html"
                                   class="btn btn-link btn-underline ls-normal text-normal font-weight-normal">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-zoom overlay-dark appear-animate"
                             data-animation-options="{'name': 'fadeInRightShorter', 'delay': '.3s'}">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{asset('main/images/demos/demo29/blog/4.jpg')}}" width="380" height="250" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    <a href="#" class="post-date"><span>on</span>Nov 22, 2018</a> | <a href="#"
                                                                                                       class="post-comment"><mark>0</mark> Comments</a>
                                </div>
                                <h3 class="post-title"><a href="#">Quisque volutpat mattis eros.</a></h3>
                                <a href="blog-classic.html"
                                   class="btn btn-link btn-underline ls-normal text-normal font-weight-normal">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="grey-section pt-10 pb-10">
                <div class="container">
                    <h2 class="title text-center d-block pt-3">Instagram</h2>
                    <div class="owl-carousel owl-theme row cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-4 pb-4"
                         data-owl-options="{
                            'items': 2,
                            'margin': 20,
                            'loop': false,
                            'responsive': {
                                '480': {
                                    'items': 2
                                },
                                '576': {
                                    'items': 3
                                },
                                '768': {
                                    'items': 4
                                },
                                '992': {
                                    'items': 6
                                }
                            }
                        }">
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/1.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/2.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/3.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/4.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/5.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                        <figure class="instagram appear-animate" data-animation-options="{
                                'name': 'fadeInUpShorter',
                                'delay': '.3s',
                                'duration': '.8s'
                            }">
                            <a href="#">
                                <img src="{{asset('main/images/demos/demo29/instagram/6.jpg')}}" alt="Instagram" width="213"
                                     height="213">
                            </a>
                        </figure>
                    </div>
                </div>
            </section>
        </div>

    </main>
    <!-- End Main -->
   @include('main.partials.footer')
    <!-- End Footer -->
</div>

<!-- Sticky Footer -->
<div class="sticky-footer sticky-content fix-bottom">
    <a href="demo29.html" class="sticky-link">
        <i class="d-icon-home"></i>
        <span>Home</span>
    </a>
    <a href="demo29-shop.html" class="sticky-link">
        <i class="d-icon-volume"></i>
        <span>Categories</span>
    </a>
    <a href="wishlist.html" class="sticky-link">
        <i class="d-icon-heart"></i>
        <span>Wishlist</span>
    </a>
    <a href="account.html" class="sticky-link">
        <i class="d-icon-user"></i>
        <span>Account</span>
    </a>
    <div class="header-search hs-toggle dir-up">
        <a href="#" class="search-toggle sticky-link">
            <i class="d-icon-search"></i>
            <span>Search</span>
        </a>
        <form action="#" class="input-wrapper">
            <input type="text" class="form-control" name="search" autocomplete="off"
                   placeholder="Search your keyword..." required />
            <button class="btn btn-search" type="submit">
                <i class="d-icon-search"></i>
            </button>
        </form>
    </div>
</div>
<!-- Scroll Top -->
<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

<!-- MobileMenu -->
<div class="mobile-menu-wrapper">
    <div class="mobile-menu-overlay">
    </div>
    <!-- End Overlay -->
    <a class="mobile-menu-close" href="#"><i class="d-icon-times"></i></a>
    <!-- End CloseButton -->
    <div class="mobile-menu-container scrollable">
        <form action="#" class="input-wrapper">
            <input type="text" class="form-control" name="search" autocomplete="off"
                   placeholder="Search your keyword..." required />
            <button class="btn btn-search" type="submit">
                <i class="d-icon-search"></i>
            </button>
        </form>
        <!-- End Search Form -->
        <ul class="mobile-menu mmenu-anim">
            <li>
                <a href="demo29.html">Home</a>
            </li>
            <li>
                <a href="demo29-shop.html">Categories</a>
                <ul>
                    <li>
                        <a href="#">
                            Variations 1
                        </a>
                        <ul>
                            <li><a href="shop-classic-filter.html">Classic Filter</a></li>
                            <li><a href="shop-left-toggle-sidebar.html">Left Toggle Filter</a></li>
                            <li><a href="shop-right-toggle-sidebar.html">Right Toggle Sidebar</a></li>
                            <li><a href="shop-horizontal-filter.html">Horizontal Filter </a>
                            </li>
                            <li><a href="shop-navigation-filter.html">Navigation Filter</a></li>

                            <li><a href="shop-off-canvas-filter.html">Off-Canvas Filter </a></li>
                            <li><a href="shop-top-banner.html">Top Banner</a></li>
                            <li><a href="shop-inner-top-banner.html">Inner Top Banner</a></li>
                            <li><a href="shop-with-bottom-block.html">With Bottom Block</a></li>
                            <li><a href="shop-category-in-page-header.html">Category In Page Header</a>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            Variations 2
                        </a>
                        <ul>
                            <li><a href="shop-grid-3cols.html">3 Columns Mode</a></li>
                            <li><a href="shop-grid-4cols.html">4 Columns Mode</a></li>
                            <li><a href="shop-grid-5cols.html">5 Columns Mode</a></li>
                            <li><a href="shop-grid-6cols.html">6 Columns Mode</a></li>
                            <li><a href="shop-grid-7cols.html">7 Columns Mode</a></li>
                            <li><a href="shop-grid-8cols.html">8 Columns Mode</a></li>
                            <li><a href="shop-list-mode.html">List Mode</a></li>
                            <li><a href="shop-pagination.html">Pagination</a></li>
                            <li><a href="shop-infinite-ajaxscroll.html">Infinite Ajaxscroll </a></li>
                            <li><a href="shop-loadmore-button.html">Loadmore Button</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            Variations 3
                        </a>
                        <ul>
                            <li><a href="shop-category-grid-shop.html">Category Grid Shop</a></li>
                            <li><a href="shop-category+products.html">Category + Products</a></li>
                            <li><a href="shop-default-1.html">Shop Default 1 </a>
                            </li>
                            <li><a href="shop-default-2.html">Shop Default 2</a></li>
                            <li><a href="shop-default-3.html">Shop Default 3</a></li>
                            <li><a href="shop-default-4.html">Shop Default 4</a></li>
                            <li><a href="shop-default-5.html">Shop Default 5</a></li>
                            <li><a href="shop-default-6.html">Shop Default 6</a></li>
                            <li><a href="shop-default-7.html">Shop Default 7</a></li>
                            <li><a href="shop-default-8.html">Shop Default 8</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="demo29-product.html">Products</a>
                <ul>
                    <li>
                        <a href="#">Product Pages</a>
                        <ul>
                            <li><a href="product-simple.html">Simple Product</a></li>
                            <li><a href="product-featured.html">Featured &amp; On Sale</a></li>
                            <li><a href="product.html">Variable Product</a></li>
                            <li><a href="product-variable-swatch.html">Variation Swatch
                                    Product</a></li>
                            <li><a href="product-grouped.html">Grouped Product </a></li>
                            <li><a href="product-external.html">External Product</a></li>
                            <li><a href="product-in-stock.html">In Stock Product</a></li>
                            <li><a href="product-out-stock.html">Out of Stock Product</a></li>
                            <li><a href="product-upsell.html">Upsell Products</a></li>
                            <li><a href="product-cross-sell.html">Cross Sell Products</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Product Layouts</a>
                        <ul>
                            <li><a href="product-vertical.html">Vertical Thumb</a></li>
                            <li><a href="product-horizontal.html">Horizontal Thumb</a></li>
                            <li><a href="product-gallery.html">Gallery Type</a></li>
                            <li><a href="product-grid.html">Grid Images</a></li>
                            <li><a href="product-masonry.html">Masonry Images</a></li>
                            <li><a href="product-sticky.html">Sticky Info</a></li>
                            <li><a href="product-sticky-both.html">Left & Right Sticky</a></li>
                            <li><a href="product-left-sidebar.html">With Left Sidebar</a></li>
                            <li><a href="product-right-sidebar.html">With Right Sidebar</a></li>
                            <li><a href="product-full.html">Full Width Layout </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Product Features</a>
                        <ul>
                            <li><a href="product-sale.html">Sale Countdown</a></li>
                            <li><a href="product-hurryup.html">Hurry Up Notification </a></li>
                            <li><a href="product-attribute-guide.html">Attribute Guide </a></li>
                            <li><a href="product-sticky-cart.html">Add Cart Sticky</a></li>
                            <li><a href="product-thumbnail-label.html">Labels on Thumbnail</a>
                            </li>
                            <li><a href="product-more-description.html">More Description
                                    Tabs</a></li>
                            <li><a href="product-accordion-data.html">Data In Accordion</a></li>
                            <li><a href="product-tabinside.html">Data Inside</a></li>
                            <li><a href="product-video.html">Video Thumbnail </a>
                            </li>
                            <li><a href="product-360-degree.html">360 Degree Thumbnail </a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Pages</a>
                <ul>
                    <li><a href="about-us.html">About</a></li>
                    <li><a href="contact-us.html">Contact Us</a></li>
                    <li><a href="account.html">Login</a></li>
                    <li><a href="faq.html">FAQs</a></li>
                    <li><a href="error-404.html">Error 404</a>
                        <ul>
                            <li><a href="error-404.html">Error 404-1</a></li>
                            <li><a href="error-404-1.html">Error 404-2</a></li>
                            <li><a href="error-404-2.html">Error 404-3</a></li>
                            <li><a href="error-404-3.html">Error 404-4</a></li>
                        </ul>
                    </li>
                    <li><a href="coming-soon.html">Coming Soon</a></li>
                </ul>
            </li>
            <li>
                <a href="blog-classic.html">Blog</a>
                <ul>
                    <li><a href="blog-classic.html">Classic</a></li>
                    <li><a href="blog-listing.html">Listing</a></li>
                    <li>
                        <a href="#">Grid</a>
                        <ul>
                            <li><a href="blog-grid-2col.html">Grid 2 columns</a></li>
                            <li><a href="blog-grid-3col.html">Grid 3 columns</a></li>
                            <li><a href="blog-grid-4col.html">Grid 4 columns</a></li>
                            <li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Masonry</a>
                        <ul>
                            <li><a href="blog-masonry-2col.html">Masonry 2 columns</a></li>
                            <li><a href="blog-masonry-3col.html">Masonry 3 columns</a></li>
                            <li><a href="blog-masonry-4col.html">Masonry 4 columns</a></li>
                            <li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Mask</a>
                        <ul>
                            <li><a href="blog-mask-grid.html">Blog mask grid</a></li>
                            <li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="post-single.html">Single Post</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="element.html">Elements</a>
                <ul>
                    <li>
                        <a href="#">Elements 1</a>
                        <ul>
                            <li><a href="element-accordions.html">Accordions</a></li>
                            <li><a href="element-alerts.html">Alert &amp; Notification</a></li>

                            <li><a href="element-banner-effect.html">Banner Effect

                                </a></li>
                            <li><a href="element-banner.html">Banner
                                </a></li>
                            <li><a href="element-blog-posts.html">Blog Posts</a></li>
                            <li><a href="element-breadcrumb.html">Breadcrumb
                                </a></li>
                            <li><a href="element-buttons.html">Buttons</a></li>
                            <li><a href="element-cta.html">Call to Action</a></li>
                            <li><a href="element-countdown.html">Countdown
                                </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Elements 2</a>
                        <ul>
                            <li><a href="element-counter.html">Counter </a></li>
                            <li><a href="element-creative-grid.html">Creative Grid

                                </a></li>
                            <li><a href="element-animation.html">Entrance Effect

                                </a></li>
                            <li><a href="element-floating.html">Floating

                                </a></li>
                            <li><a href="element-hotspot.html">Hotspot

                                </a></li>
                            <li><a href="element-icon-boxes.html">Icon Boxes</a></li>
                            <li><a href="element-icons.html">Icons</a></li>
                            <li><a href="element-image-box.html">Image box

                                </a></li>
                            <li><a href="element-instagrams.html">Instagrams</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#">Elements 3</a>
                        <ul>

                            <li><a href="element-categories.html">Product Category</a></li>
                            <li><a href="element-products.html">Products</a></li>
                            <li><a href="element-product-banner.html">Products + Banner

                                </a></li>
                            <li><a href="element-product-grid.html">Products + Grid

                                </a></li>
                            <li><a href="element-product-single.html">Product Single

                                </a>
                            </li>
                            <li><a href="element-product-tab.html">Products + Tab

                                </a></li>
                            <li><a href="element-single-product.html">Single Product

                                </a></li>
                            <li><a href="element-slider.html">Slider

                                </a></li>
                            <li><a href="element-social-link.html">Social Icons </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Elements 4</a>
                        <ul>
                            <li><a href="element-subcategory.html">Subcategory

                                </a></li>
                            <li><a href="element-svg-floating.html">Svg Floating

                                </a></li>
                            <li><a href="element-tabs.html">Tabs</a></li>
                            <li><a href="element-testimonials.html">Testimonials
                                </a></li>
                            <li><a href="element-titles.html">Title</a></li>
                            <li><a href="element-typography.html">Typography</a></li>
                            <li><a href="element-vendor.html">Vendor

                                </a></li>
                            <li><a href="element-video.html">Video

                                </a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="https://d-themes.com/buynow/riodehtml">Buy Riode!</a></li>
        </ul>
        <!-- End MobileMenu -->
    </div>
</div>

<!-- newsletter-popup default -->
<div class="newsletter-popup newsletter-pop1 mfp-hide" id="newsletter-popup"
     style="background-image: url({{asset('main/images/newsletter-popup.jpg')}})">
    <div class="newsletter-content">
        <h4 class="text-uppercase text-dark">Up to <span class="text-primary">20% Off</span></h4>
        <h2 class="font-weight-semi-bold">Sign up to <span>RIODE</span></h2>
        <p class="text-grey">Subscribe to the Riode eCommerce newsletter to receive timely updates from your
            favorite
            products.</p>
        <form action="#" method="get" class="input-wrapper input-wrapper-inline input-wrapper-round">
            <input type="email" class="form-control email" name="email" id="email2"
                   placeholder="Email address here..." required="">
            <button class="btn btn-dark" type="submit">SUBMIT</button>
        </form>
        <div class="form-checkbox justify-content-center">
            <input type="checkbox" class="custom-checkbox" id="hide-newsletter-popup" name="hide-newsletter-popup"
                   required />
            <label for="hide-newsletter-popup">Don't show this popup again</label>
        </div>
    </div>
</div>

<!-- sticky icons-->
<div class="sticky-icons-wrapper">
    <div class="sticky-icon-links">
        <ul>
            <li><a href="#" class="demo-toggle"><i class="fas fa-home"></i><span>Demos</span></a></li>
            <li><a href="documentation.html"><i class="fas fa-info-circle"></i><span>Documentation</span></a>
            </li>
            <li><a href="https://themeforest.net/downloads/"><i class="fas fa-star"></i><span>Reviews</span></a>
            </li>
            <li><a href="https://d-themes.com/buynow/riodehtml"><i class="fas fa-shopping-cart"></i><span>Buy
							now!</span></a></li>
        </ul>
    </div>
    <div class="demos-list">
        <div class="demos-overlay"></div>
        <a class="demos-close" href="#"><i class="close-icon"></i></a>
        <div class="demos-content scrollable scrollable-light">
            <h3 class="demos-title">Demos</h3>
            <div class="demos">
            </div>
        </div>
    </div>
</div>
<!-- Plugins JS File -->
<script src="{{asset('main/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('main/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
<script src="{{asset('main/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('main/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('main/vendor/jquery.plugin/jquery.plugin.min.js')}}"></script>
<script src="{{asset('main/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('main/vendor/isotope/isotope.pkgd.min.js')}}"></script>
<!-- Main JS File -->
<script src="{{asset('main/js/main.min.js')}}"></script>
</body>

</html>
