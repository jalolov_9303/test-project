<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <p class="welcome-msg">3D modellar do'koniga hush kelibsiz!</p>
            </div>
            <div class="header-right">
                <!-- End DropDown Menu -->
                <div class="dropdown ml-5">
                    <a href="#">language</a>
                    <ul class="dropdown-box">
                        <li>
                            <a href="#">uz</a>
                        </li>
                        <li>
                            <a href="#">en</a>
                        </li>
                        <li>
                            <a href="#">ru</a>
                        </li>
                    </ul>
                </div>
                <!-- End DropDown Menu -->
                <span class="divider"></span>
                <a href="contact-us.html" class="contact d-lg-show"><i class="d-icon-map"></i>Contact</a>
                <a href="#" class="help d-lg-show"><i class="d-icon-info"></i> Need Help</a>
                <a href="#signin" class="login-toggle link-to-tab d-md-show"><i class="d-icon-user"></i>Sign
                    in</a>
                <span class="delimiter">/</span>
                <a href="#register" class="register-toggle link-to-tab d-md-show ml-0">Register</a>

                <div class="dropdown login-dropdown off-canvas">
                    <div class="canvas-overlay"></div>
                    <!-- End Login Toggle -->
                    <div class="dropdown-box scrollable">
                        <div class="login-popup">
                            <div class="form-box">
                                <div class="tab tab-nav-simple tab-nav-boxed form-tab">
                                    <ul
                                        class="nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5">
                                        <li class="nav-item">
                                            <a class="nav-link active border-no lh-1 ls-normal"
                                               href="{{route('login')}}">Login</a>
                                        </li>
                                        <li class="delimiter">or</li>
                                        <li class="nav-item">
                                            <a class="nav-link border-no lh-1 ls-normal"
                                               href="{{route('register')}}">Register</a>
                                        </li>
                                    </ul>
                                    {{--                                        <div class="tab-content">--}}
                                    {{--                                            <div class="tab-pane active" id="signin">--}}
                                    {{--                                                <form action="#">--}}
                                    {{--                                                    <div class="form-group mb-3">--}}
                                    {{--                                                        <input type="text" class="form-control" id="singin-email"--}}
                                    {{--                                                               name="singin-email"--}}
                                    {{--                                                               placeholder="Username or Email Address *" required="">--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="form-group">--}}
                                    {{--                                                        <input type="password" class="form-control"--}}
                                    {{--                                                               id="singin-password" name="singin-password"--}}
                                    {{--                                                               placeholder="Password *" required="">--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="form-footer">--}}
                                    {{--                                                        <div class="form-checkbox">--}}
                                    {{--                                                            <input type="checkbox" class="custom-checkbox"--}}
                                    {{--                                                                   id="signin-remember" name="signin-remember">--}}
                                    {{--                                                            <label class="form-control-label"--}}
                                    {{--                                                                   for="signin-remember">Remember--}}
                                    {{--                                                                me</label>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                        <a href="#" class="lost-link">Lost your password?</a>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <button class="btn btn-dark btn-block btn-rounded"--}}
                                    {{--                                                            type="submit">Login</button>--}}
                                    {{--                                                </form>--}}
                                    {{--                                                <div class="form-choice text-center">--}}
                                    {{--                                                    <label class="ls-m">or Login With</label>--}}
                                    {{--                                                    <div class="social-links">--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-google fab fa-google border-no"></a>--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-facebook fab fa-facebook-f border-no"></a>--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-twitter fab fa-twitter border-no"></a>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <div class="tab-pane" id="register">--}}
                                    {{--                                                <form action="#">--}}
                                    {{--                                                    <div class="form-group mb-3">--}}
                                    {{--                                                        <input type="email" class="form-control" id="register-email"--}}
                                    {{--                                                               name="register-email" placeholder="Your Email Address *"--}}
                                    {{--                                                               required="">--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="form-group">--}}
                                    {{--                                                        <input type="password" class="form-control"--}}
                                    {{--                                                               id="register-password" name="register-password"--}}
                                    {{--                                                               placeholder="Password *" required="">--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="form-footer">--}}
                                    {{--                                                        <div class="form-checkbox">--}}
                                    {{--                                                            <input type="checkbox" class="custom-checkbox"--}}
                                    {{--                                                                   id="register-agree" name="register-agree"--}}
                                    {{--                                                                   required="">--}}
                                    {{--                                                            <label class="form-control-label" for="register-agree">I--}}
                                    {{--                                                                agree to the--}}
                                    {{--                                                                privacy policy</label>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <button class="btn btn-dark btn-block btn-rounded"--}}
                                    {{--                                                            type="submit">Register</button>--}}
                                    {{--                                                </form>--}}
                                    {{--                                                <div class="form-choice text-center">--}}
                                    {{--                                                    <label class="ls-m">or Register With</label>--}}
                                    {{--                                                    <div class="social-links">--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-google fab fa-google border-no"></a>--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-facebook fab fa-facebook-f border-no"></a>--}}
                                    {{--                                                        <a href="#" title="social-link"--}}
                                    {{--                                                           class="social-link social-twitter fab fa-twitter border-no"></a>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                </div>
                            </div>
                            <button title="Close (Esc)" type="button" class="mfp-close"><span>×</span></button>
                        </div>
                    </div>
                    <!-- End Dropdown Box -->
                </div>
                <!-- End DropDownExpanded Menu -->
            </div>
        </div>
    </div>
    <!-- End HeaderTop -->
    <div class="header-middle sticky-header fix-top sticky-content">
        <div class="container">
            <div class="header-left">
                <a href="#" class="mobile-menu-toggle">
                    <i class="d-icon-bars2"></i>
                </a>
                <a href="demo29.html" class="logo mr-4">
                    <img src="{{asset('main/images/demos/demo29/logo.png')}}" alt="logo" width="153" height="43" />
                </a>
            </div>
            <div class="header-center mr-xl-6 ml-xl-8">
                <!-- End Logo -->
                <div class="header-search hs-simple mr-0">
                    <form action="#" method="get" class="input-wrapper">
                        <div class="select-box">
                            <select id="category" name="category">
                                <option selected disabled>All Categories</option>
                            @foreach($categories as $category)
                                    <option value="{{$category->id}}" >{{$category->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="text" class="form-control" name="search" id="search"
                               placeholder="Search..." required="">
                        <button class="btn btn-sm btn-search" type="submit" title="submit-button"><i
                                class="d-icon-search"></i></button>
                    </form>
                </div>
                <!-- End Header Search -->
            </div>
            <div class="header-right">
                <a href="tel:#" class="icon-box icon-box-side mr-lg-3">
                    <div class="icon-box-icon mr-0 mr-lg-2">
                        <i class="d-icon-phone"></i>
                    </div>
                    <div class="icon-box-content d-lg-show">
                        <h4 class="icon-box-title">biz bilan bog'laning:</h4>
                        <p>+998(93) 123-45-67</p>
                    </div>
                </a>
                <span class="divider"></span>
                <div class="dropdown compare-dropdown off-canvas d-md-show mr-xl-7 mr-4">
                    <a href="#" class="compare compare-toggle mr-0" title="comapre">
                        <i class="d-icon-compare"></i>
                    </a>
                    <div class="canvas-overlay"></div>
                    <!-- End Compare Toggle -->
                    <div class="dropdown-box scrollable">
                        <div class="canvas-header">
                            <h4 class="canvas-title">Compare</h4>
                            <a href="#" class="btn btn-dark btn-link btn-icon-right btn-close">close<i
                                    class="d-icon-arrow-right"></i><span class="sr-only">Compare</span></a>
                        </div>
                        <div class="products scrollable">
                            <div class="product product-compare">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/compare/product-1.jpg')}}" alt="product" width="80"
                                             height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Riode White Trends</a>
                                    <div class="price-box">
                                                <span class="product-price">$21.00
                                                    <del class="old-price pl-1">$40.00</del>
                                                </span>
                                    </div>
                                </div>

                            </div>
                            <!-- End of Compare Product -->
                            <div class="product product-compare">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/compare/product-2.jpg')}}" alt="product" width="80"
                                             height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Dark Blue Women’s
                                        Leomora Hat</a>
                                    <div class="price-box">
                                                <span class="product-price">$118.00
                                                    <del class="old-price pl-1">$158.99</del>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Compare Product -->
                        </div>
                        <a href="compare.html" class="btn btn-dark compare-btn mt-4"><span>Go To Compare
                                        List</span></a>
                        <!-- End of Products  -->
                    </div>
                    <!-- End Dropdown Box -->
                </div>
                <div class="dropdown wishlist wishlist-dropdown off-canvas">
                    <a href="wishlist.html" class="wishlist-toggle" title="wishlist">
                        <i class="d-icon-heart"></i>
                    </a>
                    <div class="canvas-overlay"></div>
                    <!-- End Wishlist Toggle -->
                    <div class="dropdown-box scrollable">
                        <div class="canvas-header">
                            <h4 class="canvas-title">wishlist</h4>
                            <a href="#" class="btn btn-dark btn-link btn-icon-right btn-close">close<i
                                    class="d-icon-arrow-right"></i><span class="sr-only">wishlist</span></a>
                        </div>
                        <div class="products scrollable">
                            <div class="product product-wishlist">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/wishlist/product-1.jpg')}}" width="100" height="100"
                                             alt="product" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Girl's Dark Bag</a>
                                    <div class="price-box">
                                        <span class="product-price">$84.00</span>
                                    </div>
                                </div>
                            </div>
                            <!-- End of wishlist Product -->
                            <div class="product product-wishlist">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/wishlist/product-2.jpg')}}" width="100" height="100"
                                             alt="product" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Women's Fashional Comforter
                                    </a>
                                    <div class="price-box">
                                        <span class="product-price">$84.00</span>
                                    </div>
                                </div>
                            </div>
                            <!-- End of wishlist Product -->
                            <div class="product product-wishlist">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/wishlist/product-3.jpg')}}" width="100" height="100"
                                             alt="product" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Wide Knickerbockers</a>
                                    <div class="price-box">
                                        <span class="product-price">$84.00</span>
                                    </div>
                                </div>
                            </div>

                            <!-- End of wishlist Product -->
                        </div>
                        <a href="wishlist.html" class="btn btn-dark wishlist-btn mt-4"><span>Go To
                                        Wishlist</span></a>
                        <!-- End of Products  -->
                    </div>
                    <!-- End Dropdown Box -->
                </div>
                <span class="divider"></span>
                <div class="dropdown cart-dropdown type2 off-canvas mr-0 mr-lg-2">
                    <a href="#" class="cart-toggle label-block link">
                        <div class="cart-label d-lg-show ls-normal">
                            <span class="cart-name">Shopping Cart:</span>
                            <span class="cart-price">$42.00</span>
                        </div>
                        <i class="d-icon-bag"><span class="cart-count">2</span></i>
                    </a>
                    <div class="canvas-overlay"></div>
                    <!-- End Cart Toggle -->
                    <div class="dropdown-box scrollable">
                        <div class="canvas-header">
                            <h4 class="canvas-title">Shopping Cart</h4>
                            <a href="#" class="btn btn-dark btn-link btn-icon-right btn-close">close<i
                                    class="d-icon-arrow-right"></i><span class="sr-only">Cart</span></a>
                        </div>
                        <div class="products scrollable">
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/cart/product-1.jpg')}}" alt="product" width="80"
                                             height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Riode White Trends</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$21.00</span>
                                    </div>
                                </div>

                            </div>
                            <!-- End of Cart Product -->
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="product.html">
                                        <img src="{{asset('main/images/cart/product-2.jpg')}}" alt="product" width="80"
                                             height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="product.html" class="product-name">Dark Blue Women’s
                                        Leomora Hat</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$118.00</span>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Cart Product -->
                        </div>
                        <!-- End of Products  -->
                        <div class="cart-total">
                            <label>Subtotal:</label>
                            <span class="price">$139.00</span>
                        </div>
                        <!-- End of Cart Total -->
                        <div class="cart-action">
                            <a href="cart.html" class="btn btn-dark btn-link">View Cart</a>
                            <a href="checkout.html" class="btn btn-dark"><span>Go To Checkout</span></a>
                        </div>
                        <!-- End of Cart Action -->
                    </div>
                    <!-- End Dropdown Box -->
                </div>

            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="container">
            <div class="header-category-menu row gutter-no text-white w-100">
                <div class="category-menu-content scrollable scrollable-light">
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-desktop"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Computers</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-wireless"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Component</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-camera2"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Electronics</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-gamepad2" style="font-size: 3.3rem; margin-bottom: 4px;"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Game consoles</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-wifi" style="font-size: 4rem; margin: -7px auto 3px;"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Networks</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-officebag"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Office Solution</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-mobile" style="font-size: 3.7rem; margin: -3px auto 2px;"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">SmartPhone</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-bridge-lamp"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Industrial</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-headphone"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Headphones</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="demo29-shop.html">
                            <figure class="category-media">
                                <i class="d-icon-memory" style="font-size: 3.6rem; margin: -5px auto 4px;"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">Accessories</h4>
                            </div>
                        </a>
                    </div>
                    <div class="category category-icon">
                        <a href="">
                            <figure class="category-media">
                                <i class="d-icon-category"></i>
                            </figure>
                            <div class="category-content">
                                <h4 class="category-name">All Categories</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
