<footer class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="owl-carousel owl-theme owl-middle row cols-xl-4 cols-md-3 cols-sm-2 cols-2"
                 data-owl-options="{
                        'items': 3,
                        'margin': 0,
                        'dots': false,
                        'autoplay': true,
                        'responsive': {
                            '0': {
                                'items': 1
                            },
                            '576': {
                                'items': 1
                            },
                            '768': {
                                'items': 2
                            },
                            '992': {
                                'items': 3
                            }
                        }
                    }">
                <div class="icon-box icon-box-side slide-icon-box justify-content-center">
                    <i class="icon-box-icon d-icon-truck" style="font-size: 46px;">
                    </i>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Free shipping &amp; Return</h4>
                        <p>Free shipping on orders over $99</p>
                    </div>
                </div>
                <div class="icon-box icon-box-side slide-icon-box justify-content-center">
                    <i class="icon-box-icon d-icon-service">
                    </i>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Customer Support 24/7</h4>
                        <p>Instant access to perfect support</p>
                    </div>
                </div>
                <div class="icon-box icon-box-side slide-icon-box justify-content-center">
                    <i class="icon-box-icon d-icon-secure">
                    </i>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">100% Secure Payment</h4>
                        <p>We ensure secure payment!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container p-0">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="widget widget-about">
                            <a href="demo29.html" class="logo-footer">
                                <img src="{{asset('main/images/demos/demo29/logo-footer.png')}}" alt="logo-footer" width="154"
                                     height="43" />
                            </a>
                            <div class="widget-body">
                                <p>Fringilla urna porttitor rhoncus dolor purus luctus <br> venenatis lectus
                                    magna
                                    fringilla diam maecenas <br> ultricies mi eget mauris.
                                </p>
                                <a href="mailto:mail@example.com">Riode@example.com</a>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="widget">
                                    <h4 class="widget-title">About Us</h4>
                                    <ul class="widget-body">
                                        <li>
                                            <a href="about-us.html">About Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Order History</a>
                                        </li>
                                        <li>
                                            <a href="faq.html">FAQ</a>
                                        </li>
                                        <li>
                                            <a href="contact-us.html">Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Log in</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Widget -->
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="widget">
                                    <h4 class="widget-title">Customer Service</h4>
                                    <ul class="widget-body">
                                        <li>
                                            <a href="#">Payment Methods</a>
                                        </li>
                                        <li>
                                            <a href="#">Money-back Guarantee!</a>
                                        </li>
                                        <li>
                                            <a href="#">Returns</a>
                                        </li>
                                        <li>
                                            <a href="#">Shipping</a>
                                        </li>
                                        <li>
                                            <a href="#">Terms and Conditions</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Widget -->
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="widget">
                                    <h4 class="widget-title">My Account</h4>
                                    <ul class="widget-body">
                                        <li>
                                            <a href="#">Sign in</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">View Cart</a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html">My Wishlist</a>
                                        </li>
                                        <li>
                                            <a href="#">Track My Order</a>
                                        </li>
                                        <li>
                                            <a href="#">Help</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Widget -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Footer Middle -->
        <div class="footer-bottom">
            <div class="container p-0">
                <div class="footer-left">
                    <figure class="payment">
                        <img src="{{asset('main/images/payment.png')}}" alt="payment" width="159" height="29" />
                    </figure>
                </div>
                <div class="footer-center">
                    <p style="color: white" class="copyright"> <span style="color: cadetblue" >Soft Go</span> e Commerce &copy;  2023.  All Rights Reserved &nbsp; <a style="color: red ;" target="_blank" href="https://softgo.uz">Soft Go</a></p>
                </div>
                <div class="footer-right">
                    <div class="social-links">
                        <a href="#" title="social-link"
                           class="social-link social-facebook fab fa-facebook-f"></a>
                        <a href="#" title="social-link" class="social-link social-twitter fab fa-twitter"></a>
                        <a href="#" title="social-link"
                           class="social-link social-linkedin fab fa-linkedin-in mr-0"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End FooterBottom -->
    </div>
</footer>
