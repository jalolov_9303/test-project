<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmessengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amessengers', function (Blueprint $table) {
            $table->id();
            $table->integer('admin_id');
            $table->integer('architec_id');
            $table->text('admin_messege')->nullable();
            $table->text('architec_messege')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amessengers');
    }
}
